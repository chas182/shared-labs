package bsu.azabavskaya.processing;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;


public class WikiReducer extends Reducer<Text, Text, Text, Text> {
    private static final Logger logger = LoggerFactory.getLogger(WikiReducer.class);

    Text result = new Text();
    private Map<String, LinkedList<String>> resultMap = new TreeMap<>();

    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        logger.debug("Key: " + key);

        for (Text val : values) {
            String valStr = val.toString();
            if (resultMap.containsKey(valStr)) {
                LinkedList<String> list = resultMap.get(valStr);
                list.add(key.toString());
            } else {
                resultMap.put(valStr, new LinkedList<>(Collections.singleton(key.toString())));
            }
        }

        if (context.getProgress() == 1f) {
            for (String resKey : resultMap.keySet()) {
                final String[] titles = {""};
                for (String title : resultMap.get(resKey)) {
                    titles[0] += title + ";";
                }
                result.set(titles[0]);
                try {
                    context.write(new Text(resKey), result);
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
