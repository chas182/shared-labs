package bsu.azabavskaya.processing;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;

public class WikiMapper extends Mapper<LongWritable, Text, Text, Text> {
    private static final Logger logger = LoggerFactory.getLogger(WikiMapper.class);

    private Set<String> words = new TreeSet<>();
    private boolean titleFound = false;
    private Text title;

    @Override
    protected void map(LongWritable key, Text value, Context context) {
        logger.debug("Incoming '{}' -> '{}'", key, value);

        if (!titleFound) {
            title = new Text(value.toString());
            titleFound = true;
            return;
        }

        StringTokenizer itr = new StringTokenizer(value.toString(),
                ",.;:- —<>$()[]{}\"\"\\«»\n\t0123456789+=-*/!?&_^%#'~`");
        while (itr.hasMoreTokens()) {
            String str = itr.nextToken();
            words.add(str.toLowerCase());
        }

        Text text = new Text();
        words.forEach(word -> {
            text.set(word);
            try {
                context.write(title, text);
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

//    public boolean isNumeric(String str) {
//        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
//    }
}
