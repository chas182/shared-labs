package bsu.azabavskaya.util;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

public final class WikiPages {

    @SuppressWarnings("all")
    public static String generate(int pageCount, String inputPath, String outPath) {

        File inputDir = new File(inputPath);
        if (inputDir.exists()) {
            for (File f : inputDir.listFiles())
                f.delete();
        } else {
            inputDir.mkdirs();
        }
        File outDir = new File(outPath);
        if (outDir.exists()) {
            for (File f : outDir.listFiles())
                f.delete();
            outDir.delete();
        }
        for (int i = 0; i < pageCount; i++) {
            try {
                File newFile = new File(
                        inputDir + "/" +
                                UUID.randomUUID().toString() + ".txt");
                Document doc = Jsoup.connect("http://en.wikipedia.org/wiki/Special:Random").get();
                String toFile = doc.body().getElementById("firstHeading").text() + "\n" +
                        doc.body().getElementById("bodyContent").text();
                newFile.createNewFile();
                try (FileOutputStream fos = new FileOutputStream(newFile)) {
                    fos.write(toFile.getBytes());
                } catch (IOException e) {
                    System.out.println("Error while copying file");
                }
                System.out.println("File " + newFile + " created.");
            } catch (Exception e) {
                i--;
            }
        }

        return inputPath;
    }
}
