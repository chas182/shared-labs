package bsu.vabramov.par.lab;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class MyMapper extends Mapper<Text, Text, Text, DoubleWritable> {
    private static final Logger logger = LoggerFactory.getLogger(MyMapper.class);

    private DoubleWritable val = new DoubleWritable();

    @Override
    protected void map(Text key, Text value, Context context) throws IOException, InterruptedException {
        logger.debug("Incoming '{}' -> '{}'", key, value);

        val.set(Double.valueOf(value.toString()));
        context.write(key, val);
    }
}
