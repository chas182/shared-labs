package bsu.vabramov.par.lab;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;


public class MyReducer extends Reducer<Text, DoubleWritable, Text, Text> {
    private static final Logger logger = LoggerFactory.getLogger(MyReducer.class);

    Text result = new Text();
    private Map<String, LinkedList<String>> resultMap = new TreeMap<>();

    @Override
    protected void reduce(Text key, Iterable<DoubleWritable> values, Context context) throws IOException, InterruptedException {
        logger.debug("Key: " + key);
        final double[] sum = {0};
        final double[] disp = {0};
        LinkedList<Double> list = new LinkedList<>();
        for (DoubleWritable val : values) {
            list.add(val.get());
        }
        list.forEach(val-> sum[0] += val);

        double average = sum[0] / list.size();
        list.forEach(val-> {
            double a = val - average;
            disp[0] += a * a;
        });
        disp[0] /= list.size();
        result.set("" + average + "\t" + disp[0]);
        context.write(key, result);
    }
}
