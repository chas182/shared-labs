package bsu.vabramov.par.lab;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.PrimitiveIterator;
import java.util.Random;
import java.util.UUID;

public final class StatisticGenerator {


    @SuppressWarnings("all")
    public static String generate(int pageCount, String inputPath, String outPath) {

        File inputDir = new File(inputPath);
        if (inputDir.exists()) {
            for (File f : inputDir.listFiles())
                f.delete();
        } else {
            inputDir.mkdirs();
        }
        File outDir = new File(outPath);
        if (outDir.exists()) {
            for (File f : outDir.listFiles())
                f.delete();
            outDir.delete();
        }
        String[] keys = new String[pageCount];
        for (int i = 0; i < keys.length; ++i)
            keys[i] = "sequence-" + (i + 1);
        PrimitiveIterator.OfInt ints = new Random().ints(0, keys.length).iterator();
        PrimitiveIterator.OfDouble doubles = new Random().doubles(0, 1).iterator();

        for (int i = 0; i < pageCount; ++i) {
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j < 10000; ++j) {
                sb.append(keys[ints.nextInt()]);
                sb.append("\t");
                sb.append(doubles.nextDouble());
                sb.append("\n");
            }
            try {
                File newFile = new File(
                        inputDir + "/" +
                                UUID.randomUUID().toString() + ".txt");
                newFile.createNewFile();
                try (FileOutputStream fos = new FileOutputStream(newFile)) {
                    fos.write(sb.toString().getBytes());
                } catch (IOException e) {
                    System.out.println("Error while copying file");
                }
                System.out.println("File " + newFile + " created.");
            } catch (Exception e) {
                i--;
            }

        }

        return inputPath;
    }
}
