package bsu.zabavskaya.sclab1.util;

import java.awt.*;
import java.awt.image.BufferedImage;

public class LabUtils {
    /**
     * Converts a given Image into a BufferedImage
     *
     * @param img The Image to be converted
     * @return The converted BufferedImage
     */
    public static BufferedImage toBufferedImage(Image img) {
        if (img instanceof BufferedImage) {
            return (BufferedImage) img;
        }

        // Create a buffered image with transparency
        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

        // Draw the image on to the buffered image
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();

        // Return the buffered image
        return bimage;
    }

    public static int[] getDataForBrightnessDiagram(BufferedImage image) {
        int[] hist = new int[256];
        int width = image.getWidth();
        int height = image.getHeight();
        int[] pixels = new int[width * height];
        image.getRGB(0, 0, width, height, pixels, 0, width);

        for (int p : pixels) {
            int r = 0xff & (p >> 16);
            int g = 0xff & (p >> 8);
            int b = 0xff & (p);
            int y = (int) (.33 * r + .56 * g + .11 * b);
            hist[y]++;
        }

        return hist;
    }

    public static int[][] getPixelsMatrix(BufferedImage image) {
        int width = image.getWidth();
        int height = image.getHeight();
        int[] pixels = new int[width * height];
        int[][] matrix = new int[width][height];
        image.getRGB(0, 0, width, height, pixels, 0, width);

        for (int i = 0; i < height; ++i)
            for (int j = 0; i < width; ++j)
                matrix[i][j] = pixels[i*height + width];

        return matrix;
    }
}
