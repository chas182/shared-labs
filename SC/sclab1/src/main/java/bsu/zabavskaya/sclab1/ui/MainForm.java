package bsu.zabavskaya.sclab1.ui;

import bsu.zabavskaya.sclab1.panel.DrawPanel;
import bsu.zabavskaya.sclab1.util.LabUtils;

import javax.swing.*;
import java.io.File;
import java.util.Arrays;

public class MainForm extends JFrame {

    private BrightnessDiagramDialog diagramDialog = new BrightnessDiagramDialog(this);
    private JFileChooser fc = new JFileChooser(); 
    {
        String path = new File("").getAbsolutePath();
        path = path.substring(0, path.indexOf("shared-labs"));
        fc.setCurrentDirectory(new File(path));
    }

    public MainForm() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        drawPanel = new DrawPanel();
        menuBar = new javax.swing.JMenuBar();
        fileMenuItem = new javax.swing.JMenu();
        fileOpenMI = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        exitMI = new javax.swing.JMenuItem();
        toolsMenuItem = new javax.swing.JMenu();
        brightnessDiagramMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setLocationByPlatform(true);
        setResizable(false);

        drawPanel.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout drawPanelLayout = new javax.swing.GroupLayout(drawPanel);
        drawPanel.setLayout(drawPanelLayout);
        drawPanelLayout.setHorizontalGroup(
            drawPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 900, Short.MAX_VALUE)
        );
        drawPanelLayout.setVerticalGroup(
            drawPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 606, Short.MAX_VALUE)
        );

        fileMenuItem.setText("Файл");

        fileOpenMI.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        fileOpenMI.setText("Открыть ...");
        fileOpenMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileOpenMIActionPerformed(evt);
            }
        });
        fileMenuItem.add(fileOpenMI);
        fileMenuItem.add(jSeparator1);

        exitMI.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        exitMI.setText("Выход");
        exitMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitMIActionPerformed(evt);
            }
        });
        fileMenuItem.add(exitMI);

        menuBar.add(fileMenuItem);

        toolsMenuItem.setText("Иструменты");

        brightnessDiagramMenuItem.setText("Диграмма яркости");
        brightnessDiagramMenuItem.setEnabled(false);
        brightnessDiagramMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                brightnessDiagramMenuItemActionPerformed(evt);
            }
        });
        toolsMenuItem.add(brightnessDiagramMenuItem);

        menuBar.add(toolsMenuItem);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(drawPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(drawPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void fileOpenMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fileOpenMIActionPerformed
        if (JFileChooser.APPROVE_OPTION == fc.showOpenDialog(this)) {
            getDrawPanel().setImageFile(fc.getSelectedFile());
            brightnessDiagramMenuItem.setEnabled(true);
            if (diagramDialog.isVisible()) {
                diagramDialog.setData(LabUtils.getDataForBrightnessDiagram(getDrawPanel().getImage()));
            }
        }
    }//GEN-LAST:event_fileOpenMIActionPerformed

    private void exitMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMIActionPerformed
        System.exit(0);
    }//GEN-LAST:event_exitMIActionPerformed

    private void brightnessDiagramMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_brightnessDiagramMenuItemActionPerformed
        diagramDialog.setData(LabUtils.getDataForBrightnessDiagram(getDrawPanel().getImage()));
    }//GEN-LAST:event_brightnessDiagramMenuItemActionPerformed

    private DrawPanel getDrawPanel() {
        return (DrawPanel) drawPanel;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem brightnessDiagramMenuItem;
    private javax.swing.JPanel drawPanel;
    private javax.swing.JMenuItem exitMI;
    private javax.swing.JMenu fileMenuItem;
    private javax.swing.JMenuItem fileOpenMI;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenu toolsMenuItem;
    // End of variables declaration//GEN-END:variables
}
