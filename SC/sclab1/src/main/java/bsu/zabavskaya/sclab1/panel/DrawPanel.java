package bsu.zabavskaya.sclab1.panel;

import bsu.zabavskaya.sclab1.util.LabUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class DrawPanel extends JPanel {
    private static final Logger logger = LoggerFactory.getLogger(DrawPanel.class);

    private File imageFile;
    private BufferedImage image;

    public void setImageFile(File imageFile) {
        this.imageFile = imageFile;
        try {
            image = ImageIO.read(imageFile);
            image = LabUtils.toBufferedImage(image.getScaledInstance(getWidth(), getHeight(), Image.SCALE_SMOOTH));
            getGraphics().drawImage(image, 0, 0, null);
        } catch (IOException e) {
            logger.error("Error in image reading: {}", e.getMessage());
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (image != null) {
            g.drawImage(image, 0, 0, null);
        }
    }

    public BufferedImage getImage() {
        return this.image;
    }
}
