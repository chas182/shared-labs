package bsu.azabavskaya.iis.util;

public final class TextUtils {

    public static boolean isEmpty(String str) {
        return str == null || "".equals(str);
    }

    private TextUtils() {
    }
}
