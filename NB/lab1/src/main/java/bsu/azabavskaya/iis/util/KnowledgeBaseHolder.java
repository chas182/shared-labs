package bsu.azabavskaya.iis.util;

import bsu.azabavskaya.iis.core.AnalyzerLoadingData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public final class KnowledgeBaseHolder {

    private static final Logger logger = LoggerFactory.getLogger(KnowledgeBaseHolder.class);
    private static KnowledgeBaseHolder instance = new KnowledgeBaseHolder();

    private List<AnalyzerLoadingData> baseList;

    public static KnowledgeBaseHolder getInstance() {
        return instance;
    }

    public static void init() {
        try {
            init(new ClassPathResource("KnowledgeBase.txt").getFile());
        } catch (IOException e) {
            logger.error("Could not found default 'KnowledgeBase.txt'");
        }
    }

    public static void init(File sourceFile) {
        instance.baseList = FileParser.parse(sourceFile);
    }

    public static Object[] getClasses() {
        Set<String> classes = new TreeSet<>();
        for (AnalyzerLoadingData data : instance.baseList)
            classes.add(data.getClassOfInstrument());
        return classes.toArray();
    }

    public static Object[] getTypes() {
        Set<String> types = new TreeSet<>();
        for (AnalyzerLoadingData data : instance.baseList)
            types.add(data.getTypeOfInstrument());
        return types.toArray();
    }

    public static Object[] getSybTypes() {
        Set<String> subTypes = new TreeSet<>();
        for (AnalyzerLoadingData data : instance.baseList)
            subTypes.add(data.getSubTypeOfInstrument());
        return subTypes.toArray();
    }

    public static void dumpKnowledgeBase() {
        if (instance.baseList == null) {
            logger.error("Knowledge base in not initialized");
            return;
        }

        StringBuilder sb = new StringBuilder("\n--------- Knowledge base dump --------\n");
        for (AnalyzerLoadingData ad : instance.baseList) {
            sb.append(ad.toString()).append('\n');
        }
        sb.append("-------------- End dump --------------");
        logger.debug(sb.toString());
    }

    public static Answer findAnswer(Object classObj, Object typeObj, Object subtypeObj) {
        String classStr = classObj != null ? classObj.toString() : "";
        String typeStr = typeObj != null ? typeObj.toString() : "";
        String subtypeStr = subtypeObj != null ? subtypeObj.toString() : "";

        Answer foundAnswer = new Answer(Answer.AnswerType.FOUND);
        for (AnalyzerLoadingData data : instance.baseList) {
            if (data.getClassOfInstrument().equals(classStr)) {
                String dataType = data.getTypeOfInstrument();
                if (TextUtils.isEmpty(dataType)) {
                    return foundAnswer.setResult(data.getResult());
                } else if (dataType.equals(typeStr)) {
                    String dataSubtype = data.getSubTypeOfInstrument();
                    if (TextUtils.isEmpty(dataSubtype)) {
                        return foundAnswer.setResult(data.getResult());
                    } else if (dataSubtype.equals(subtypeStr)) {
                        return foundAnswer.setResult(data.getResult());
                    }
                }
            }
        }

        return Answer.NOT_FOUND;
    }
}
