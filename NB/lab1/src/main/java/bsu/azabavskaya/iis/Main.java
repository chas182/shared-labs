package bsu.azabavskaya.iis;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Alinka on 3/31/2015.
 */
public class Main {

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new bsu.azabavskaya.iis.ui.Window().setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.exit(-1);
                }
            }
        });
    }
}
