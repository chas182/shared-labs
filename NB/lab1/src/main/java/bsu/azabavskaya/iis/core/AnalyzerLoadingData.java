package bsu.azabavskaya.iis.core;

import bsu.azabavskaya.iis.util.TextUtils;

/**
 * Created by Alinka on 3/31/2015.
 */
public class AnalyzerLoadingData {

    private String classOfInstrument = "";
    private String typeOfInstrument = "";
    private String subTypeOfInstrument = "";
    private String result = "";

    public String getClassOfInstrument() {
        return classOfInstrument;
    }

    public void setClassOfInstrument(String classOfInstrument) {
        this.classOfInstrument = classOfInstrument;
    }

    public String getTypeOfInstrument() {
        return typeOfInstrument;
    }

    public void setTypeOfInstrument(String typeOfInstrument) {
        this.typeOfInstrument = typeOfInstrument;
    }

    public String getSubTypeOfInstrument() {
        return subTypeOfInstrument;
    }

    public void setSubTypeOfInstrument(String subTypeOfInstrument) {
        this.subTypeOfInstrument = subTypeOfInstrument;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AnalyzerLoadingData)) return false;

        AnalyzerLoadingData that = (AnalyzerLoadingData) o;

        if (classOfInstrument != null ? !classOfInstrument.equals(that.classOfInstrument) : that.classOfInstrument != null)
            return false;
        if (typeOfInstrument != null ? !typeOfInstrument.equals(that.typeOfInstrument) : that.typeOfInstrument != null)
            return false;
        if (subTypeOfInstrument != null ? !subTypeOfInstrument.equals(that.subTypeOfInstrument) : that.subTypeOfInstrument != null)
            return false;
        return !(result != null ? !result.equals(that.result) : that.result != null);

    }

    @Override
    public int hashCode() {
        int result = classOfInstrument != null ? classOfInstrument.hashCode() : 0;
        result = 31 * result + (typeOfInstrument != null ? typeOfInstrument.hashCode() : 0);
        result = 31 * result + (subTypeOfInstrument != null ? subTypeOfInstrument.hashCode() : 0);
        result = 31 * result + (this.result != null ? this.result.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return (!TextUtils.isEmpty(classOfInstrument) ? "class - " + classOfInstrument : "") +
                (!TextUtils.isEmpty(typeOfInstrument) ? " + type - " + typeOfInstrument : "") +
                (!TextUtils.isEmpty(subTypeOfInstrument) ? " + subtype - " + subTypeOfInstrument : "") +
                " = " + result;
    }
}
