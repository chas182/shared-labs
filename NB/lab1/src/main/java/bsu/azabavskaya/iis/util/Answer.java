package bsu.azabavskaya.iis.util;

public class Answer {

    public static final Answer NOT_FOUND = new Answer(AnswerType.NOT_FOUND, "Ответ не найден");

    private AnswerType type;
    private String result;

    public Answer(AnswerType type) {
        this.type = type;
    }

    public Answer(AnswerType type, String result) {
        this.type = type;
        this.result = result;
    }

    public Answer setResult(String result) {
        this.result = result;
        return this;
    }

    public AnswerType getType() {
        return type;
    }

    public String getResult() {
        return result;
    }

    public enum AnswerType {
        FOUND, NOT_FOUND
    }

}
