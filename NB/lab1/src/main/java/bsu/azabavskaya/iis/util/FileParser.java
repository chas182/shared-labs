package bsu.azabavskaya.iis.util;

import bsu.azabavskaya.iis.core.AnalyzerLoadingData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alinka on 4/2/2015.
 */
public final class FileParser {

    private static final Logger logger = LoggerFactory.getLogger(FileParser.class);

    public static List<AnalyzerLoadingData> parse(File sourseFile) {
        List<AnalyzerLoadingData> data = new ArrayList<>();
        logger.info("Start parse knowledge base with file '{}'", sourseFile);
        try (BufferedReader reader = new BufferedReader(new FileReader(sourseFile))) {
            String line;
            StringBuilder term = new StringBuilder();
            boolean merge = false;
            while ((line = reader.readLine()) != null) {
                AnalyzerLoadingData loadingData = new AnalyzerLoadingData();
                line = new String(line.getBytes(), "UTF-8");
                String trimedLine = line.trim();
                String[] rules = trimedLine.split(";");
                boolean termEnd = trimedLine.endsWith(";");
                int start = 0;
                int end = termEnd ? rules.length : rules.length - 1;
                if (merge) {
                    term.append(rules[0]);
                    if (rules.length == 1 && !termEnd) {
                        continue;
                    } else {
                        start = 1;
                        merge = false;
                        dispatchRuleCreation(term.toString(), loadingData);
                        term.setLength(0);
                    }
                }
                for (int i = start; i < end; i++) {
                    dispatchRuleCreation(rules[i], loadingData);
                }
                if (!termEnd) {
                    merge = true;
                    term.append(rules[end]);
                }
                data.add(loadingData);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    private static void dispatchRuleCreation(String rule, AnalyzerLoadingData loadingData) {
        String[] ruleSides = rule.split("=");
        if(ruleSides.length < 2) {
            System.err.printf("rule '%s' syntax error! skipping the rule...\n", rule);
        }
        parseFirstRulePart(ruleSides[0], loadingData);
        loadingData.setResult(ruleSides[1].trim());
    }

    private static void parseFirstRulePart(String subrule, AnalyzerLoadingData loadingData) {
        String[] components = subrule.trim().split("[+]");
        switch (components.length) {
            case 3:
                loadingData.setSubTypeOfInstrument(components[2].trim().split("-")[1].trim());
            case 2:
                loadingData.setTypeOfInstrument(components[1].trim().split("-")[1].trim());
            case 1:
                loadingData.setClassOfInstrument(components[0].trim().split("-")[1].trim());
                break;
            default:
                break;
        }
    }
}
