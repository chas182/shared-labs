класс - струнные + тип - клавишные = рояль;
класс - струнные + тип - щипковые = гитара;
класс - струнные + тип - смычковые = скрипка;
класс - язычковые = аккордеон;
класс - духовые + тип - тростевые = саксофон;
класс - духовые + тип - лабиальные = флейта;
класс - ударные + тип - мембранные = барабаны;
класс - ударные + тип - пластиночные +подтип - настраеваемые = ксилофон;
класс - ударные + тип - пластиночные +подтип - ненастраеваемые = тарелки;
класс - ударные + тип - шумовые + = треугольник;