package bsu.azabavskaya.iis.lab1.core;

public enum ResultType {
    KEY, VALUE, ERROR, ANSWER;
}
