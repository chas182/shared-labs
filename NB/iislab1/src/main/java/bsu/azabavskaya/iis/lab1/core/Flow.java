package bsu.azabavskaya.iis.lab1.core;

import bsu.azabavskaya.iis.lab1.rules.*;

import java.util.ArrayDeque;

public class Flow {

    private ArrayDeque<RulesNest> stack;
    private ForwardIndex fw;
    private BackwardIndex bw;
    private RulesNest currentRules = null;
    private String currentRequest = null;
    private String currentFlow = "";
    private StringBuffer result;
    private int col = 0;

    public Flow(ForwardIndex fw, BackwardIndex bw) {
        this.fw = fw;
        this.bw = bw;
        this.stack = new ArrayDeque<>();
        result = new StringBuffer();
    }

    public String getCurrentFlow() {
        return currentFlow;
    }

    public void concatCurrentFlow(String str) {
        currentFlow = currentFlow.concat(str);
    }

    public void clearCurrentFlow() {
        currentFlow = "";
    }

    public void clear() {
        stack = new ArrayDeque<>();
        fw.clean();
        bw.clean();
        currentRules = null;
        currentRequest = null;
    }

    public Result nextStep(Result request) {
        ResultType type = request.getType();
        if (type == ResultType.KEY) {
            String req = request.getRequest();
            request.setResponseCategory("��� ������ ������?");
            request.setResponse(fw.getAvailableOptions(req));
            request.setResponse(bw.getAvailableOptions());
            request.setType(ResultType.VALUE);
        } else if (type == ResultType.VALUE) {
            String req = request.getRequest();
            if (col == 0) {
                result.append("����: ").append(req).append("\r\n");
            }
            col++;
            if (req == null) {
                request.setResponse("Bad request");
            } else {
                if (currentRequest == null) {
                    currentRequest = req;
                    currentRules = bw.findMatchingRules(currentRequest);
                    result.append("������� ��� ����: \r\n");
                    currentFlow = currentFlow.concat(" -> ");
                    if (currentRules == null || currentRules.rules == null) {
                        result.append("�� ����������. \r\n");
                        FileSave.logToFile("log.txt", result.toString());
                        return null;
                    }

                    for (int i = 0; i < currentRules.rules.size(); i++) {
                        result.append(currentRules.rules.get(i)).append("\r\n");
                    }
                    result.append("\r\n");
                    FileSave.logToFile("log.txt", result.toString());
                } else {
                    fw.satisfyFact(currentRequest, req, FactStatus.TRUE);
                }
                while (true) {
                    Fact f = currentRules == null ? null : currentRules.getUnknownFact(fw);
                    if (f != null) {
                        RulesNest rn = bw.findMatchingRules(f.getLeft());
                        currentRequest = f.getLeft();
                        if (rn == null) {
                            result.append("������ ���: ").append(f.getLeft()).append("\r\n");
                            FileSave.logToFile("log.txt", result.toString());

                            request.setResponseCategory(String.format("�������� �������� ��� '%s':", currentRequest));
                            request.setResponseCategory(String.format("%s?", currentRequest));
                            currentFlow = currentFlow.concat(" " + currentRequest + "=");
                            request.setResponse(fw.getAvailableOptions(currentRequest));
                            break;
                        } else {
                            stack.push(currentRules);
                            currentRules = rn;
                            result.append("���������� ������� ��� ���������� ����:" + "\r\n");
                            for (int i = 0; i < rn.rules.size(); i++) {
                                //System.out.println(rn.bsu.vabramov.iis.lab1.rules.get(i));
                                result.append(rn.rules.get(i)).append("\r\n");
                            }
                            result.append("\r\n");
                            FileSave.logToFile("log.txt", result.toString());

                        }
                    } else {
                        if (stack.size() == 0) {
                            String answer;
                            if (currentRules != null && currentRules.getCorrectRule() != null) {
                                answer = currentRules.getCorrectRule().getConclusion();
                            } else {
                                answer = "����� �� ����� ���� ������";
                                result.append("����� �� ����� ���� ������");
                                FileSave.logToFile("log_file.txt", result.toString());
                            }
                            result.append("�����: ").append(answer).append("\r\n\r\n");
                            FileSave.logToFile("log.txt", result.toString());

                            request.setResponseCategory("�����");
                            request.setType(ResultType.ANSWER);
                            request.setResponse(answer);
                            col = 0;
                            break;
                        } else {
                            if (currentRules.getCorrectRule() != null) {
                                currentFlow = currentFlow.concat(" // " + currentRules.getCorrectRule().toString() + " // ");
                                result.append("��������� ��������� �������: ").append(currentRules.getCorrectRule().toString()).append("\r\n");
                                result.append("\r\n");
                            } else {
                                currentFlow = currentFlow.concat(" // ���������� ������ �� �������");
                                result.append("���������� ������ �� �������\r\n");
                                result.append("\r\n");
                            }
                            currentRules = stack.pop();
                        }
                    }
                }
            }
        }
        return request;
    }
}
