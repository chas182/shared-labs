package bsu.azabavskaya.iis.lab1.frame;

import bsu.azabavskaya.iis.lab1.core.Flow;
import bsu.azabavskaya.iis.lab1.core.Result;
import bsu.azabavskaya.iis.lab1.core.ResultType;
import bsu.azabavskaya.iis.lab1.rules.RulesParser;
import org.springframework.core.io.ClassPathResource;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class MainFrame extends JFrame implements ActionListener {

    public static int MAX_ANSW_SIZE = 8;
    private JLabel statusBar = new JLabel("");
    private JLabel totalRulesBar = new JLabel("  ������ ���������: 0");
    private JPanel centralPanel = new JPanel();
    private JPanel northPanel = new JPanel();
    private JPanel southPanel = new JPanel();
    private JButton select = new JButton("������");
    private JLabel curFlow = new JLabel();
    private Result cc;
    private JRadioButton[] selButtons = new JRadioButton[MAX_ANSW_SIZE];
    private JFileChooser fileChooser = new JFileChooser();
    private Flow currentFlow;

    public MainFrame(String path) {
        setTitle("��� ������������ �1");
        setSize(500, 350);
        setLocationByPlatform(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        currentFlow = reloadKnowledgeBase(path);

        select.addActionListener(this);

        JMenuBar menuBar = new JMenuBar();
        JMenu mainMenu = new JMenu("��������");
        final JMenuItem itemStart = new JMenuItem("������");
        final JMenuItem itemReload = new JMenuItem("�������� ����");
        final JMenuItem itemExit = new JMenuItem("�����");

        mainMenu.add(itemStart);
        mainMenu.add(itemReload);
        mainMenu.add(itemExit);
        menuBar.add(mainMenu);
        setJMenuBar(menuBar);

        itemExit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        itemReload.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int ret = fileChooser.showOpenDialog(MainFrame.this);
                if (ret == JFileChooser.APPROVE_OPTION) {
                    File f = fileChooser.getSelectedFile();
                    try {
                        currentFlow = reloadKnowledgeBase(f.getAbsolutePath());
                        cc = null;
                        cleanCentralPanel();
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                    itemStart.setEnabled(currentFlow != null);
                }
            }
        });

        itemStart.setEnabled(currentFlow != null);
        itemStart.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                cc = new Result(null);
                if (select != null) {
                    select.setVisible(true);
                    select.setEnabled(true);
                    currentFlow.clear();
                    cc = currentFlow.nextStep(cc);
                    doSetView();
                }
                currentFlow.clearCurrentFlow();
                curFlow.setText("");
            }
        });

        northPanel.setLayout(new GridLayout(2, 1));
//        northPanel.setBorder(BorderFactory.createEtchedBorder());
        northPanel.add(totalRulesBar);
        northPanel.add(statusBar);

        centralPanel.setLayout(new GridLayout(MAX_ANSW_SIZE + 1, 1));

        ButtonGroup selGroup = new ButtonGroup();
        for (int i = 0; i < MAX_ANSW_SIZE; i++) {
            selButtons[i] = new JRadioButton();
            selGroup.add(selButtons[i]);
            centralPanel.add(selButtons[i]);
        }

        JPanel b = new JPanel(new GridLayout(1, 3));
        b.add(new JPanel());
        b.add(select);
        b.add(new JPanel());

        southPanel.add(curFlow);

        centralPanel.add(b);
        add(northPanel, BorderLayout.NORTH);
        add(centralPanel, BorderLayout.CENTER);
        add(southPanel, BorderLayout.SOUTH);

        cleanCentralPanel();
    }

    public static void main(String args[]) {
        String path = MainFrame.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        try {
            final String rulesFile = path. substring(0, path.indexOf("iislab1")) + System.getProperty("file.separator") +  "KnowledgeBase.txt";
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    new MainFrame(rulesFile).setVisible(true);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Flow reloadKnowledgeBase(String pathToBase) {
        if (pathToBase != null) {
            RulesParser parser = new RulesParser(pathToBase);
            parser.parse();
//            statusBar.setText("���� ������ ���������");
            totalRulesBar.setText(String.format("  ������ ���������: %d", parser.getRulesCount()));
            return new Flow(parser.getForwardIndex(), parser.getBackwardIndex());
        }
        return null;
    }

    private void cleanCentralPanel() {
        for (JRadioButton rb : selButtons) {
            rb.setVisible(false);
        }
        select.setVisible(false);
    }

    private void doSetView() {
        for (int i = 0; i < MAX_ANSW_SIZE; i++) {
            selButtons[i].setVisible(false);
        }
        if (cc.getType() == ResultType.ANSWER) {
            select.setEnabled(false);
            statusBar.setText("<html><h4> ���������: " + cc.getResponse().get(0)+"</h4></html>");
        } else {
            statusBar.setText("<html><h4>" + cc.getResponseCategory() + "</h4></html>");
            int i = 0;
            selButtons[0].setSelected(true);
            for (String category : cc.getResponse()) {
                selButtons[i].setText(category);
                selButtons[i].setVisible(true);
                i++;
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        for (JRadioButton rb : selButtons) {
            if (rb.isSelected()) {
                cc.setRequest(rb.getText());
                currentFlow.concatCurrentFlow(rb.getText());
                curFlow.setText(currentFlow.getCurrentFlow());
                break;
            }
        }
        cc = currentFlow.nextStep(cc);
        doSetView();
    }
}
