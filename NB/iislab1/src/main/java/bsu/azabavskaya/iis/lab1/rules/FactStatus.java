package bsu.azabavskaya.iis.lab1.rules;

public enum FactStatus {
    TRUE, FALSE, NO_INFO;
}
