package bsu.azabavskaya.iis.lab1.rules;

import java.util.ArrayList;
import java.util.List;

public class Rule {
    private List<Fact> conditions;
    private List<Fact> conclusions;

    public Rule(List<Fact> conditions, List<Fact> conclusions) {
        this.conclusions = conclusions;
        this.conditions = conditions;
    }

    public List<Fact> getUnknownFacts() {
        List<Fact> unknownFacts = new ArrayList<Fact>();
        for (Fact f : conditions) {
            if (f.getStatus() == FactStatus.NO_INFO) {
                unknownFacts.add(f);
            } else if (f.getStatus() == FactStatus.FALSE) {
                return null;
            }
        }
        return unknownFacts;
    }

    public boolean checkRule(ForwardIndex fw) {
        boolean satisfiedConditions = true;
        for (Fact f : conditions) {
            satisfiedConditions = satisfiedConditions && f.getStatus() == FactStatus.TRUE;
        }
        for (Fact f : conclusions) {
            fw.satisfyFact(f.getLeft(), f.getRight(), satisfiedConditions ? FactStatus.TRUE : FactStatus.FALSE);
        }
        return satisfiedConditions;
    }

    public String getConclusion() {
        return this.conclusions.get(0).getRight();
    }

    @Override
    public String toString() {
        StringBuilder strB = new StringBuilder();
        rulesToStr(rulesToStr(strB, this.conditions).append(" THEN "), this.conclusions);
        return strB.toString();
    }

    private StringBuilder rulesToStr(StringBuilder strB, List<Fact> list) {
        for (int i = 0, leng = list.size(); i < leng; i++) {
            if (i > 0) {
                strB.append(" AND ");
            }
            strB.append(list.get(i).toString());
        }
        return strB;
    }

}
