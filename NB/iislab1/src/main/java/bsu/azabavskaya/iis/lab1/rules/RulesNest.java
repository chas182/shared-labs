package bsu.azabavskaya.iis.lab1.rules;

import java.util.ArrayList;
import java.util.List;

public class RulesNest {
    public List<Rule> rules;
    private Rule correctRule;

    public RulesNest() {
        rules = new ArrayList<Rule>();
    }

    public void putToNest(Rule r) {
        rules.add(r);
    }

    public Fact getUnknownFact(ForwardIndex fw) {
        List<Fact> unkn = null;
        for (Rule r : rules) {
            unkn = r.getUnknownFacts();
            if (unkn != null && unkn.size() != 0) {
                break;
            }
            if (r.checkRule(fw)) {
                this.correctRule = r;
            }
        }
        return unkn == null || unkn.size() == 0 ? null : unkn.get(0);
    }

    public Rule getCorrectRule() {
        return correctRule;
    }

    public void clear() {
        correctRule = null;
    }

}
