package bsu.azabavskaya.iis.lab1.core;

import java.io.*;

public class FileSave {
    private String str;

    public FileSave(String fileName, String result) {
        PrintWriter writer;
        try {
            writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(fileName), "UTF-8"));
            writer.write(result);
            writer.close();
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void logToFile(String fileName, String result) {
        PrintWriter writer;
        try {
            writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(fileName), "UTF-8"));
            writer.write(result);
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getStr() {
        return str;
    }
}
