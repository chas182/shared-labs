package bsu.azabavskaya.iis.lab1.rules;

public class Fact {
    private String left;
    private String right;
    private FactStatus status = FactStatus.NO_INFO;

    public Fact() {
        this(null, null);
    }

    public Fact(String left, String right) {
        this.setLeft(left);
        this.setRight(right);
    }

    public String getLeft() {
        return left;
    }

    public void setLeft(String left) {
        this.left = left;
    }

    public String getRight() {
        return right;
    }

    public void setRight(String right) {
        this.right = right;
    }

    public FactStatus getStatus() {
        return status;
    }

    public void setStatus(FactStatus status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Fact)) return false;

        Fact fact = (Fact) o;

        return !(left != null ? !left.equals(fact.left) : fact.left != null) && !(right != null ? !right.equals(fact.right) : fact.right != null);

    }

    @Override
    public int hashCode() {
        int result = left != null ? left.hashCode() : 0;
        result = 31 * result + (right != null ? right.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.format("%s: %s", left, right);
    }

}
