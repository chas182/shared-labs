package bsu.azabavskaya.iis.lab1.rules;

import java.util.LinkedHashMap;
import java.util.Set;

public class BackwardIndex {

    private LinkedHashMap<String, RulesNest> index;

    public BackwardIndex() {
        this.index = new LinkedHashMap<String, RulesNest>();
    }

    public void registerRule(String right, Rule r) {
        RulesNest matchingRules = index.get(right);
        if (matchingRules == null) {
            matchingRules = new RulesNest();
            index.put(right, matchingRules);
        }
        matchingRules.putToNest(r);
    }

    public void clean() {
        for (String key : index.keySet()) {
            RulesNest nest = index.get(key);
            nest.clear();
        }
    }

    public RulesNest findMatchingRules(String right) {
        return index.get(right);
    }

    public Set<String> getAvailableOptions() {
        return index.keySet();
    }
}
