package bsu.azabavskaya.iis.lab1.rules;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Set;

public class ForwardIndex {
    private LinkedHashMap<String, LinkedHashMap<String, Fact>> index;
    private Set<Fact> facts;

    public ForwardIndex() {
        this.index = new LinkedHashMap<>();
        this.facts = new LinkedHashSet<>();
    }

    public Fact createRule(String left, String right) {
        LinkedHashMap<String, Fact> keys = index.get(left);
        Fact currentFact;
        if (keys != null) {
            currentFact = keys.get(right);
            if (currentFact != null) {
                return currentFact;
            }
        } else {
            keys = new LinkedHashMap<String, Fact>();
            index.put(left, keys);
        }
        currentFact = new Fact(left, right);
        keys.put(right, currentFact);
        facts.add(currentFact);
        return currentFact;
    }

    public Set<String> getAvailableOptions(String left) {
        if (left == null) {
            return index.keySet();
        } else {
            LinkedHashMap<String, Fact> options = index.get(left);
            if (options != null) {
                return options.keySet();
            } else {
                return null;
            }
        }
    }

    public void satisfyFact(String left, String right, FactStatus status) {
        LinkedHashMap<String, Fact> options = index.get(left);
        try {
            for (String key : options.keySet()) {
                if (status == FactStatus.TRUE) {
                    options.get(key).setStatus(
                            key.equals(right) ? FactStatus.TRUE : FactStatus.FALSE);
                } else if (status == FactStatus.FALSE && key.equals(right)) {
                    options.get(key).setStatus(FactStatus.FALSE);
                }
            }
        } catch (NullPointerException e) {
            System.err.printf("Bad reference to fact: %s = %s", left, right);
        }
    }

    public void clean() {
        for (Fact f : facts) {
            f.setStatus(FactStatus.NO_INFO);
        }
    }
}
