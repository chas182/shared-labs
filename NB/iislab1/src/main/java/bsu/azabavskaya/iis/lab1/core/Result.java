package bsu.azabavskaya.iis.lab1.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Result {

    private ResultType type;
    private String responseCategory;
    private List<String> response;
    private String request;

    public Result(String request) {
        this.request = request;
        this.type = ResultType.KEY;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public List<String> getResponse() {
        return response;
    }

    public void setResponse(String response) {
        List<String> l = new ArrayList<>();
        l.add(response);
        this.response = l;
    }

    public void setResponse(Collection<String> response) {
        if (this.response == null) {
           this.response = new ArrayList<>();
        }
        this.response.clear();
        this.response.addAll(response);
    }

    public ResultType getType() {
        return type;
    }

    public void setType(ResultType type) {
        this.type = type;
    }

    public String getResponseCategory() {
        return responseCategory;
    }

    public void setResponseCategory(String responseCategory) {
        this.responseCategory = responseCategory;
    }
}
