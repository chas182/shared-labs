-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: lab1_db
-- ------------------------------------------------------
-- Server version	5.6.22-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book` (
  `book_id` int(11) NOT NULL,
  `name` varchar(256) DEFAULT NULL,
  `author` varchar(256) DEFAULT NULL,
  `publishing` varchar(256) DEFAULT NULL,
  `selling_price` int(11) DEFAULT NULL,
  `purchase_price` int(11) DEFAULT NULL,
  PRIMARY KEY (`book_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` VALUES (1,'Мастер и Маргорита','М.А.Булгаков','Центрполиграф',100000,70000),(2,'20000 лье под водой','Жуль Верн','Государственное издательство художественной литературы',120000,100000),(3,'Маленький Принц','Антуана де Сент-Экзюпери','Эдисьон Гаймар',15000,10000),(4,'Война и мир','Л.Н.Толстой','Эксмо',200000,180000),(5,'Алые Паруса','Грин','ЭКСМО',20000,18000),(6,'Анна Каренина','Л.Н.Толстой','ЭКСМО',200000,180000),(7,'Ромео и Джульетта','Шекспир','Бертельсманн Медиа Москау',200000,180000),(8,'Пиковая Дама','А.С.Пушкин','Бертельсманн Медиа Москау',20000,18000),(9,'Герой Нашего Времени','М.Ю. Лермонтов','ОлмаМедиаГрупп',30000,28000),(10,'Преступление наказание','Ф.М.Достоевский','Эльбрус',50000,48000);
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `books_per_transfer`
--

DROP TABLE IF EXISTS `books_per_transfer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `books_per_transfer` (
  `books_per_transfer_id` int(11) NOT NULL,
  `number_of_books` int(11) DEFAULT NULL,
  `book_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`books_per_transfer_id`),
  KEY `from_book_to_book_per_transfer` (`book_id`),
  CONSTRAINT `books_per_transfer_ibfk_1` FOREIGN KEY (`book_id`) REFERENCES `book` (`book_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `books_per_transfer`
--

LOCK TABLES `books_per_transfer` WRITE;
/*!40000 ALTER TABLE `books_per_transfer` DISABLE KEYS */;
INSERT INTO `books_per_transfer` VALUES (1,12,6),(2,7,8),(3,8,7),(4,6,4),(5,3,2),(6,2,1),(7,4,3),(8,5,5),(9,3,5),(10,7,9);
/*!40000 ALTER TABLE `books_per_transfer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `residue`
--

DROP TABLE IF EXISTS `residue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `residue` (
  `residue_id` int(11) NOT NULL,
  `count_of_book` int(11) DEFAULT NULL,
  `seller_id` int(11) DEFAULT NULL,
  `book_id` int(11) DEFAULT NULL,
  `is_store` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`residue_id`),
  KEY `from_seller_to_residuel` (`seller_id`),
  KEY `from_book_to_residue` (`book_id`),
  CONSTRAINT `residue_ibfk_1` FOREIGN KEY (`seller_id`) REFERENCES `seller` (`seller_id`),
  CONSTRAINT `residue_ibfk_3` FOREIGN KEY (`book_id`) REFERENCES `book` (`book_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `residue`
--

LOCK TABLES `residue` WRITE;
/*!40000 ALTER TABLE `residue` DISABLE KEYS */;
INSERT INTO `residue` VALUES (1,20,1,4,0),(2,5,2,1,0),(3,7,7,NULL,NULL),(4,3,2,NULL,NULL),(5,8,4,NULL,NULL),(6,5,7,NULL,NULL),(7,7,NULL,10,1),(8,9,NULL,8,1),(9,2,NULL,7,1),(10,8,NULL,3,1);
/*!40000 ALTER TABLE `residue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seller`
--

DROP TABLE IF EXISTS `seller`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seller` (
  `seller_id` int(11) NOT NULL,
  `name` varchar(256) DEFAULT NULL,
  `address` varchar(256) DEFAULT NULL,
  `percentage_of_realization` int(11) DEFAULT NULL,
  PRIMARY KEY (`seller_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seller`
--

LOCK TABLES `seller` WRITE;
/*!40000 ALTER TABLE `seller` DISABLE KEYS */;
INSERT INTO `seller` VALUES (1,'Иван Петрович','ул.Семашко д.5',10),(2,'Иван Васильев','ул.Мазурова д.25',5),(3,'Владислав Мудрый','ул.Сердича д.60',15),(4,'Андрей Яковлев','ул.Захарова д.50',15),(5,'Михаил Ефимов','ул.Матусевича д.75',1),(6,'Евгений Мирный','ул.Притыцкого д.7',17),(7,'Евгений Иванов','ул.Жудро д.9',18),(8,'Александр Тихонов','ул.Ольшевского д.10',13),(9,'Александр Догелев','ул.Паномаренко д.10',18),(10,'Александр Догелев','ул.Темерязево д.65',25);
/*!40000 ALTER TABLE `seller` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `waybill`
--

DROP TABLE IF EXISTS `waybill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `waybill` (
  `id_waybill` int(11) NOT NULL,
  `number` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `type` varchar(256) DEFAULT NULL,
  `books_per_transfer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_waybill`),
  KEY `from_book_per_transfer_to_waybill` (`books_per_transfer_id`),
  CONSTRAINT `waybill_ibfk_1` FOREIGN KEY (`books_per_transfer_id`) REFERENCES `books_per_transfer` (`books_per_transfer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `waybill`
--

LOCK TABLES `waybill` WRITE;
/*!40000 ALTER TABLE `waybill` DISABLE KEYS */;
INSERT INTO `waybill` VALUES (1,1,'2025-10-20','склад-продавец',NULL),(2,2,'2026-10-20','продавец-продавец',NULL),(3,3,'2027-10-20','продавец-продавец',NULL),(4,4,'2028-10-20','продавец-продавец',NULL),(5,5,'2029-10-20','склад-продавец',NULL),(6,6,'2030-10-20','продавец-продавец',NULL),(7,7,'2030-10-20','продавец-продавец',NULL),(8,8,'2031-10-20','продавец-продавец',NULL),(9,9,'2031-10-20','продавец-продавец',NULL),(10,10,'2031-10-20','склад-продавец',NULL);
/*!40000 ALTER TABLE `waybill` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-05-11 13:17:55