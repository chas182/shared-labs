DROP TABLE waybill;
DROP TABLE books_per_transfer;
DROP TABLE residue;
DROP TABLE book;
DROP TABLE seller;

CREATE TABLE book (
  book_id INTEGER PRIMARY KEY NOT NULL,
  name VARCHAR(256) DEFAULT NULL,
  author VARCHAR(256) DEFAULT NULL,
  publishing VARCHAR(256) DEFAULT NULL,
  selling_price INTEGER DEFAULT NULL,
  purchase_price INTEGER DEFAULT NULL
);
commit;

CREATE TABLE books_per_transfer (
  books_per_transfer_id INTEGER PRIMARY KEY NOT NULL,
  number_of_books INTEGER DEFAULT NULL,
  book_id INTEGER DEFAULT NULL,
  CONSTRAINT books_per_transfer_ibfk_1 FOREIGN KEY (book_id) REFERENCES book (book_id)
);
commit;

CREATE TABLE seller (
  seller_id INTEGER PRIMARY KEY NOT NULL,
  name VARCHAR(256) DEFAULT NULL,
  address VARCHAR(256) DEFAULT NULL,
  percentage_of_realization INTEGER DEFAULT NULL
);
commit;

CREATE TABLE residue (
  residue_id INTEGER PRIMARY KEY NOT NULL,
  count_of_book INTEGER DEFAULT NULL,
  seller_id INTEGER DEFAULT NULL,
  book_id INTEGER DEFAULT NULL,
  is_store char check (is_store in (0,1)),
  FOREIGN KEY (seller_id) REFERENCES seller (seller_id),
  FOREIGN KEY (book_id) REFERENCES book (book_id)
);
commit;

CREATE TABLE waybill (
  id_waybill INTEGER PRIMARY KEY NOT NULL,
  waybill_number INTEGER DEFAULT NULL,
  data DATE DEFAULT NULL,
  type VARCHAR(256) DEFAULT NULL,
  books_per_transfer_id INTEGER DEFAULT NULL,
  CONSTRAINT waybill_ibfk_1 FOREIGN KEY (books_per_transfer_id) REFERENCES books_per_transfer (books_per_transfer_id)
);
commit;

INSERT INTO book VALUES (1,'Мастер и Маргорита','М.А.Булгаков','Центрполиграф',100000,70000);
INSERT INTO book VALUES (2,'20000 лье под водой','Жуль Верн','Гос. изд. худ. лит.',120000,100000);
INSERT INTO book VALUES (3,'Маленький Принц','Антуана де Сент-Экзюпери','Эдисьон Гаймар',15000,10000);
INSERT INTO book VALUES (4,'Война и мир','Л.Н.Толстой','Эксмо',200000,180000);
INSERT INTO book VALUES (5,'Алые Паруса','Грин','ЭКСМО',20000,18000);
INSERT INTO book VALUES (6,'Анна Каренина','Л.Н.Толстой','ЭКСМО',200000,180000);
INSERT INTO book VALUES (7,'Ромео и Джульетта','Шекспир','Бертельсманн Медиа Москау',200000,180000);
INSERT INTO book VALUES (8,'Пиковая Дама','А.С.Пушкин','Бертельсманн Медиа Москау',20000,18000);
INSERT INTO book VALUES (9,'Герой Нашего Времени','М.Ю. Лермонтов','ОлмаМедиаГрупп',30000,28000);
INSERT INTO book VALUES (10,'Преступление наказание','Ф.М.Достоевский','Эльбрус',50000,48000);
commit;

INSERT INTO books_per_transfer VALUES (1,12,6);
INSERT INTO books_per_transfer VALUES (2,7,8);
INSERT INTO books_per_transfer VALUES (3,8,7);
INSERT INTO books_per_transfer VALUES (4,6,4);
INSERT INTO books_per_transfer VALUES (5,3,2);
INSERT INTO books_per_transfer VALUES (6,2,1);
INSERT INTO books_per_transfer VALUES (7,4,3);
INSERT INTO books_per_transfer VALUES (8,5,5);
INSERT INTO books_per_transfer VALUES (9,3,5);
INSERT INTO books_per_transfer VALUES (10,7,9);
commit;

INSERT INTO seller VALUES (1,'Алексей Петрович','ул.Семашко д.5',10);
INSERT INTO seller VALUES (2,'Богдан Васильев','ул.Мазурова д.25',5);
INSERT INTO seller VALUES (3,'Владислав Мудрый','ул.Сердича д.60',15);
INSERT INTO seller VALUES (4,'Андрей Яковлев','ул.Захарова д.50',15);
INSERT INTO seller VALUES (5,'Михаил Ефимов','ул.Матусевича д.75',1);
INSERT INTO seller VALUES (6,'Евгений Мирный','ул.Притыцкого д.7',17);
INSERT INTO seller VALUES (7,'Евгений Петров','ул.Жудро д.9',18);
INSERT INTO seller VALUES (8,'Александр Тихонов','ул.Ольшевского д.10',13);
INSERT INTO seller VALUES (9,'Александр Догелев','ул.Паномаренко д.10',18);
INSERT INTO seller VALUES (10,'Александр Догелев','ул.Темерязево д.65',25);
commit;

INSERT INTO residue VALUES (1,20,1,4,0);
INSERT INTO residue VALUES (2,5,2,1,0);
INSERT INTO residue VALUES (3,7,7,NULL,0);
INSERT INTO residue VALUES (4,3,2,NULL,0);
INSERT INTO residue VALUES (5,8,4,NULL,0);
INSERT INTO residue VALUES (6,5,7,NULL,0);
INSERT INTO residue VALUES (7,7,NULL,10,1);
INSERT INTO residue VALUES (8,9,NULL,8,1);
INSERT INTO residue VALUES (9,2,NULL,7,1);
INSERT INTO residue VALUES (10,8,NULL,3,1);
commit;

INSERT INTO waybill VALUES (1,1,to_date('2025-10-20','yyyy-mm-dd'),'склад-продавец',1);
INSERT INTO waybill VALUES (2,2,to_date('2026-10-20','yyyy-mm-dd'),'продавец-продавец',2);
INSERT INTO waybill VALUES (3,3,to_date('2027-10-20','yyyy-mm-dd'),'продавец-продавец',3);
INSERT INTO waybill VALUES (4,4,to_date('2028-10-20','yyyy-mm-dd'),'продавец-продавец',4);
INSERT INTO waybill VALUES (5,5,to_date('2029-10-20','yyyy-mm-dd'),'склад-продавец',5);
INSERT INTO waybill VALUES (6,6,to_date('2030-10-20','yyyy-mm-dd'),'продавец-продавец',6);
INSERT INTO waybill VALUES (7,7,to_date('2030-10-20','yyyy-mm-dd'),'продавец-продавец',7);
INSERT INTO waybill VALUES (8,8,to_date('2031-10-20','yyyy-mm-dd'),'продавец-продавец',8);
INSERT INTO waybill VALUES (9,9,to_date('2031-10-20','yyyy-mm-dd'),'продавец-продавец',9);
INSERT INTO waybill VALUES (10,10,to_date('2031-10-20','yyyy-mm-dd'),'склад-продавец',10);
commit;