package bsu.zabavskaya.db.service;

/**
 * @author Vlad Abramov
 * @since 5/25/2015
 */

import bsu.zabavskaya.db.repo.*;
import bsu.zabavskaya.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class StubInit {

    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private BookPerTransferRepository bookPerTransferRepository;
    @Autowired
    private SellerRepository sellerRepository;
    @Autowired
    private ResidueRepository residueRepository;
    @Autowired
    private WaybillRepository waybillRepository;

    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public void init() {
        List<Book> books = Arrays.asList(
                new Book("Мастер и Маргорита", "М.А.Булгаков", "Центрполиграф", 100000, 70000),
                new Book("20000 лье под водой", "Жуль Верн", "Гос. изд. худ. лит.", 120000, 100000),
                new Book("Маленький Принц", "Антуана де Сент-Экзюпери", "Эдисьон Гаймар", 15000, 10000),
                new Book("Война и мир", "Л.Н.Толстой", "Эксмо", 200000, 180000),
                new Book("Алые Паруса", "Грин", "ЭКСМО", 20000, 18000),
                new Book("Анна Каренина", "Л.Н.Толстой", "ЭКСМО", 200000, 180000),
                new Book("Ромео и Джульетта", "Шекспир", "Бертельсманн Медиа Москау", 200000, 180000),
                new Book("Пиковая Дама", "А.С.Пушкин", "Бертельсманн Медиа Москау", 20000, 18000),
                new Book("Герой Нашего Времени", "М.Ю. Лермонтов", "ОлмаМедиаГрупп", 30000, 28000),
                new Book("Преступление наказание", "Ф.М.Достоевский", "Эльбрус", 50000, 48000)
        );
        bookRepository.save(books);

        List<BookPerTransfer> booksPerTransfer = Arrays.asList(
                new BookPerTransfer(12, books.get(5)),
                new BookPerTransfer(7, books.get(7)),
                new BookPerTransfer(8, books.get(6)),
                new BookPerTransfer(6, books.get(3)),
                new BookPerTransfer(3, books.get(1)),
                new BookPerTransfer(2, books.get(0)),
                new BookPerTransfer(4, books.get(2)),
                new BookPerTransfer(5, books.get(4)),
                new BookPerTransfer(3, books.get(4)),
                new BookPerTransfer(7, books.get(8))
        );
        booksPerTransfer.forEach(bookPerTransferRepository::save);

        List<Seller> sellers = Arrays.asList(
                new Seller("Алексей Петрович", "ул.Семашко д.5", 10),
                new Seller("Богдан Васильев", "ул.Мазурова д.25", 5),
                new Seller("Владислав Мудрый", "ул.Сердича д.60", 15),
                new Seller("Андрей Яковлев", "ул.Захарова д.50", 15),
                new Seller("Михаил Ефимов", "ул.Матусевича д.75", 1),
                new Seller("Евгений Мирный", "ул.Притыцкого д.7", 17),
                new Seller("Евгений Петров", "ул.Жудро д.9", 18),
                new Seller("Александр Тихонов", "ул.Ольшевского д.10", 13),
                new Seller("Александр Догелев", "ул.Паномаренко д.10", 18),
                new Seller("Александр Иванов", "ул.Темерязево д.65", 25)
        );
        sellers.forEach(sellerRepository::save);

        List<Residue> residues = Arrays.asList(
                new Residue(20, sellers.get(0), books.get(3), false),
                new Residue(5, sellers.get(1), books.get(4), false),
                new Residue(7, sellers.get(6), books.get(0), false),
                new Residue(3, sellers.get(1), books.get(3), false),
                new Residue(8, sellers.get(3), books.get(5), false),
                new Residue(5, sellers.get(6), books.get(1), false),
                new Residue(7, null, books.get(9), true),
                new Residue(9, null, books.get(7), true),
                new Residue(2, null, books.get(6), true),
                new Residue(8, null, books.get(2), true)
        );
        residues.forEach(residueRepository::save);

        List<Waybill> waybills = Arrays.asList(
                new Waybill(1, new Date(2025 - 1990, 9, 20), Waybill.Type.fromString("склад-продавец"), booksPerTransfer.get(0)),
                new Waybill(2, new Date(2026 - 1990, 9, 20), Waybill.Type.fromString("продавец-продавец"), booksPerTransfer.get(1)),
                new Waybill(3, new Date(2027 - 1990, 9, 20), Waybill.Type.fromString("продавец-продавец"), booksPerTransfer.get(2)),
                new Waybill(4, new Date(2028 - 1990, 9, 20), Waybill.Type.fromString("продавец-продавец"), booksPerTransfer.get(3)),
                new Waybill(4, new Date(2029 - 1990, 9, 20), Waybill.Type.fromString("склад-продавец"), booksPerTransfer.get(4)),
                new Waybill(4, new Date(2030 - 1990, 9, 20), Waybill.Type.fromString("продавец-продавец"), booksPerTransfer.get(5)),
                new Waybill(4, new Date(2030 - 1990, 9, 20), Waybill.Type.fromString("продавец-продавец"), booksPerTransfer.get(6)),
                new Waybill(4, new Date(2031 - 1990, 9, 20), Waybill.Type.fromString("продавец-продавец"), booksPerTransfer.get(7)),
                new Waybill(4, new Date(2031 - 1990, 9, 20), Waybill.Type.fromString("продавец-продавец"), booksPerTransfer.get(8)),
                new Waybill(4, new Date(2031 - 1990, 9, 20), Waybill.Type.fromString("склад-продавец"), booksPerTransfer.get(9))
        );
        waybills.forEach(waybillRepository::save);
    }
}
