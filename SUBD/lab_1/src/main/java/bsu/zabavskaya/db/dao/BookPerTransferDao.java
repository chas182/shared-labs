package bsu.zabavskaya.db.dao;

import bsu.zabavskaya.model.Book;
import bsu.zabavskaya.model.BookPerTransfer;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class BookPerTransferDao extends AbstractDao<BookPerTransfer> {
    private static final String TABLE_NAME = "books_per_transfer";
    private static final String QUERY_SELECT_ALL = "SELECT * FROM books_per_transfer ";
    private static final String QUERY_UPDATE_BOOK = "UPDATE books_per_transfer SET book_id=?, number_of_books=? WHERE books_per_transfer_id=?";

    private static final String[] columnNames = new String[] {
            "books_per_transfer_id",
            "number_of_books",
            "book_id"
    };

//    private static final RowMapper<BookPerTransfer> BOOK_PER_TRANSFER_ROW_MAPPER = new RowMapper<BookPerTransfer>() {
//        @Override
//        public BookPerTransfer mapRow(ResultSet rs, int rowNum) throws SQLException {
//            return new BookPerTransfer(
//                    rs.getInt(columnNames[0]),
//                    rs.getInt(columnNames[1]),
//                    rs.getInt(columnNames[2])
//            );
//        }
//    };

    private BookPerTransferDao (JdbcTemplate jdbcTemplate){
        super(jdbcTemplate);
    }
//
//    @Override
//    public void save(BookPerTransfer obj) {
//
//    }
//
//    @Override
//    public String getTableName() {
//        return TABLE_NAME;
//    }
//
//    @Override
//    public RowMapper<BookPerTransfer> getRowMapper() {
//        return BOOK_PER_TRANSFER_ROW_MAPPER;
//    }
//
//    @Override
//    protected String getQueryUpdate() {
//        return QUERY_UPDATE_BOOK;
//    }
//
//    public Map<String, Object> getInsertMapParams(BookPerTransfer obj) {
//        Map<String, Object> params = super.getInsertMapParams(obj);
//        params.put(columnNames[1], obj.getNumberOfBooks());
//        params.put(columnNames[2], obj.getBookId());
//        return params;
//    }
//    @Override
//    protected Object[] getUpdateParams(BookPerTransfer obj) {
//        return new Object[]{
//                obj.getBookId(),
//                obj.getNumberOfBooks(),
//                obj.getId()
//        };
//    }
//
//    @Override
//    protected String getIdColumnName() {
//        return columnNames[0];
//    }
//
//    public List<BookPerTransfer> getAll() {
//        return getJdbcTemplate().query(
//                QUERY_SELECT_ALL,
//                BOOK_PER_TRANSFER_ROW_MAPPER
//        );
//    }
}
