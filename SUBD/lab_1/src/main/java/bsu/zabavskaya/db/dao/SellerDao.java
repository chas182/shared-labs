package bsu.zabavskaya.db.dao;

import bsu.zabavskaya.model.Seller;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.io.UnsupportedEncodingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class SellerDao extends AbstractDao<Seller> {
    private static final String TABLE_NAME = "Seller";
    private static final String QUERY_SELECT_SELLER = "SELECT * FROM Seller ";
    private static final String QUERY_UPDATE_Seller = "UPDATE Seller SET name=?,address=?,percentage_of_realization=? WHERE seller_id=?";

    private static final String[] columnNames = new String[] {
            "seller_id",
            "name",
            "address",
            "percentage_of_realization"
    };

//    private static final RowMapper<Seller> SELLER_ROW_MAPPER = new RowMapper<Seller>() {
//        @Override
//        public Seller mapRow(ResultSet rs, int rowNum) throws SQLException {
//            try {
//                return new Seller(
//                        rs.getInt(columnNames[0]),
//                        new String(rs.getString(columnNames[1]).getBytes("cp1251"), "utf-8"),
//                        new String(rs.getString(columnNames[2]).getBytes("cp1251"), "utf-8"),
//                        rs.getInt(columnNames[3])
//                );
//            } catch (UnsupportedEncodingException e) {
//                return null;
//            }
//        }
//    };

    public SellerDao(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

//    @Override
//    public void save(Seller obj) {
//
//    }
//
//    @Override
//    public String getTableName() {
//        return TABLE_NAME;
//    }
//
//    @Override
//    public RowMapper<Seller> getRowMapper() {
//        return SELLER_ROW_MAPPER;
//    }
//
//    @Override
//    public Map<String, Object> getInsertMapParams(Seller obj) {
//        Map<String, Object> params = super.getInsertMapParams(obj);
//        params.put(columnNames[1], obj.getName());
//        params.put(columnNames[2], obj.getAddress());
//        params.put(columnNames[3], obj.getPercentageOfRealization());
//        return params;
//    }
//
//    @Override
//    protected String getQueryUpdate() {
//        return QUERY_UPDATE_Seller;
//    }
//
//    @Override
//    protected Object[] getUpdateParams(Seller obj) {
//        return new Object[]{
//                obj.getName(),
//                obj.getAddress(),
//                obj.getPercentageOfRealization(),
//                obj.getId()
//        };
//    }
//
//    @Override
//    protected String getIdColumnName() {
//        return columnNames[0];
//    }
//
//    public List<Seller> getAll() {
//        return getJdbcTemplate().query(
//                QUERY_SELECT_SELLER,
//                SELLER_ROW_MAPPER
//        );
//    }
}
