package bsu.zabavskaya.db.service;

import bsu.zabavskaya.db.dao.WaybillDao;
import bsu.zabavskaya.db.repo.WaybillRepository;
import bsu.zabavskaya.model.Waybill;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class WaybillService {

    @Autowired
    private WaybillRepository waybillRepository;

    @Transactional(readOnly = true, propagation = Propagation.NESTED)
    public List<Waybill> getAll() {
        return waybillRepository.findAll();
    }


    @Transactional(readOnly = true, propagation = Propagation.NESTED)
    public List<Waybill> getAllBetweenDates() { //'2014-11-01' and '2014-11-17'
        return waybillRepository.findByDateBetweenAndType(
                new Date(2014-1990, 10, 1),
                new Date()/*new Date(2014-1990, 10, 17)*/,
                Waybill.Type.SEllER_STORE);
    }
    @Transactional(propagation = Propagation.REQUIRED)
    public void save(Waybill waybill) {
        waybillRepository.save(waybill);
        waybill.setNumber(waybill.getId());
        waybillRepository.save(waybill);
    }
}
