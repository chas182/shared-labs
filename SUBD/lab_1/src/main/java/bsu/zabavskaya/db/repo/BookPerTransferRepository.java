package bsu.zabavskaya.db.repo;

import bsu.zabavskaya.model.BookPerTransfer;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @author Vlad Abramov
 * @since 5/25/2015
 */
public interface BookPerTransferRepository extends CrudRepository<BookPerTransfer, Integer> {

    List<BookPerTransfer> findAll();
}
