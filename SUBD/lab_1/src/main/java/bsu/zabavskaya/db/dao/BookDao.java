package bsu.zabavskaya.db.dao;

import bsu.zabavskaya.model.Book;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.io.UnsupportedEncodingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class BookDao extends AbstractDao<Book> {
    private static final String TABLE_NAME = "Book";
    private static final String QUERY_SELECT_BOOK = "SELECT * FROM Book ";
    private static final String QUERY_UPDATE_BOOK = "UPDATE Book SET name=?,author=?,publishing=?,selling_price=?,purchase_price=? WHERE book_id=?";

    private static final String[] columnNames = new String[] {
            "book_id",
            "name",
            "author",
            "publishing",
            "selling_price",
            "purchase_price"
    };
//
//    private static final RowMapper<Book> BOOK_ROW_MAPPER = new RowMapper<Book>() {
//        @Override
//        public Book mapRow(ResultSet rs, int rowNum) throws SQLException {
//            try {
//                return new Book(
//                        rs.getInt(columnNames[0]),
//                        new String(rs.getString(columnNames[1]).getBytes("cp1251"), "utf-8"),
//                        new String(rs.getString(columnNames[2]).getBytes("cp1251"), "utf-8"),
//                        new String(rs.getString(columnNames[3]).getBytes("cp1251"), "utf-8"),
//                        rs.getInt(columnNames[4]),
//                        rs.getInt(columnNames[5])
//                );
//            } catch (UnsupportedEncodingException e) {
//                return null;
//            }
//        }
//    };

    public BookDao(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

//    @Override
//    public void save(Book obj) {
//
//    }
//
//    @Override
//    public String getTableName() {
//        return TABLE_NAME;
//    }
//
//    @Override
//    public RowMapper<Book> getRowMapper() {
//        return BOOK_ROW_MAPPER;
//    }
//
//    @Override
//    public Map<String, Object> getInsertMapParams(Book obj) {
//        Map<String, Object> params = super.getInsertMapParams(obj);
//        params.put(columnNames[1], obj.getName());
//        params.put(columnNames[2], obj.getAuthor());
//        params.put(columnNames[3], obj.getPublishing());
//        params.put(columnNames[4], obj.getSellingPrice());
//        params.put(columnNames[5], obj.getPurchasePrice());
//        return params;
//    }
//
//    @Override
//    protected String getQueryUpdate() {
//        return QUERY_UPDATE_BOOK;
//    }
//
//    @Override
//    protected Object[] getUpdateParams(Book obj) {
//        String name = obj.getName();
//        String author = obj.getAuthor();
//        try {
//            name = new String(name.getBytes("utf-8"), "cp1251");
//            author = new String(author.getBytes("utf-8"), "cp1251");
//        } catch (UnsupportedEncodingException ignored) { }
//        return new Object[]{
//                name,
//                author,
//                obj.getPublishing(),
//                obj.getSellingPrice(),
//                obj.getPurchasePrice(),
//                obj.getId()
//        };
//    }
//
//    @Override
//    protected String getIdColumnName() {
//        return columnNames[0];
//    }
//
//    public List<Book> getAll() {
//        return getJdbcTemplate().query(
//                QUERY_SELECT_BOOK,
//                BOOK_ROW_MAPPER
//        );
//    }
}
