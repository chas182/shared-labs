package bsu.zabavskaya.db.repo;

import bsu.zabavskaya.model.Book;
import bsu.zabavskaya.model.Residue;
import bsu.zabavskaya.model.Seller;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @author Vlad Abramov
 * @since 5/25/2015
 */
public interface ResidueRepository extends CrudRepository<Residue, Integer> {

    List<Residue> findBySeller(Seller seller);
    List<Residue> findByBook(Book book);
    Residue findBySellerAndBook(Seller seller, Book book);
}
