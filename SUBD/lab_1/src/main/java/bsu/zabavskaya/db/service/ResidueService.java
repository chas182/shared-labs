package bsu.zabavskaya.db.service;

import bsu.zabavskaya.db.dao.BookDao;
import bsu.zabavskaya.db.dao.ResidueDao;
import bsu.zabavskaya.db.dao.SellerDao;
import bsu.zabavskaya.db.repo.BookRepository;
import bsu.zabavskaya.db.repo.ResidueRepository;
import bsu.zabavskaya.db.repo.SellerRepository;
import bsu.zabavskaya.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class ResidueService {
    @Autowired
    private SellerRepository sellerRepository;
    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private ResidueRepository residueRepository;

    @Transactional(readOnly = true, propagation = Propagation.NESTED)
    public List<BookMetadata> getBookMetadata(Book book) {
        List<Residue> residueList = residueRepository.findByBook(book);
        List<BookMetadata> metadataList = new ArrayList<>();
        for (Residue residue : residueList) {
            metadataList.add(
                    new BookMetadata(
                            residue.getCountOfBook(),
                            residue.getSeller() == null ? null : sellerRepository.findOne(residue.getSeller().getId())));
        }
        return metadataList;
    }

    @Transactional(readOnly = true, propagation = Propagation.NESTED)
    public List<SellerMetadata> getSellerMetadata(Seller seller) {
        List<Residue> residueList = residueRepository.findBySeller(seller);
        List<SellerMetadata> metadataList = new ArrayList<>();
        for (Residue residue : residueList) {
            metadataList.add(
                    new SellerMetadata(
                            residue.getCountOfBook(),
                            residue.getBook() == null ? null : bookRepository.findOne(residue.getBook().getId())));
        }
        return metadataList;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void save(Residue residue) {
        residueRepository.save(residue);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void doTransferOperation(Seller sender, Seller receiver, Book book, Integer countOfBook) {
        Residue senderBookResidue = residueRepository.findBySellerAndBook(sender == null ? null : sender, book);

        if (senderBookResidue != null) {
            int result = senderBookResidue.getCountOfBook() - countOfBook;
            if (result == 0) {
                residueRepository.delete(senderBookResidue);
            } else {
                senderBookResidue.setCountOfBook(result);
                residueRepository.save(senderBookResidue);
            }
        }

        Residue receiverBookResidue = residueRepository.findBySellerAndBook(receiver == null ? null : receiver, book);
        if (receiverBookResidue == null) {
            residueRepository.save(new Residue(countOfBook, receiver == null ? null : receiver, book, receiver == null));
        } else {
            receiverBookResidue.setCountOfBook(receiverBookResidue.getCountOfBook() + countOfBook);
            residueRepository.save(receiverBookResidue);
        }

    }
}
