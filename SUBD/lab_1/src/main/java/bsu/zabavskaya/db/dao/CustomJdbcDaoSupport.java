package bsu.zabavskaya.db.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class CustomJdbcDaoSupport extends JdbcDaoSupport {

    private SimpleJdbcInsert jdbcInsert;

    public CustomJdbcDaoSupport(JdbcTemplate jdbcTemplate) {
        setJdbcTemplate(jdbcTemplate);
        setJdbcInsert(new SimpleJdbcInsert(jdbcTemplate));
    }

    public SimpleJdbcInsert getJdbcInsert() {
        return jdbcInsert;
    }

    public void setJdbcInsert(SimpleJdbcInsert jdbcInsert) {
        this.jdbcInsert = jdbcInsert;
    }
}
