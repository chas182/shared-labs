package bsu.zabavskaya.db.dao;

import bsu.zabavskaya.model.Residue;
import oracle.jdbc.OracleTypes;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class ResidueDao extends AbstractDao<Residue> {
    private static final String TABLE_NAME = "Residue";
    private static final String QUERY_SELECT_RESIDUE = "SELECT * FROM Residue ";
    private static final String SELECT_RESIDUE_BY_BOOK = QUERY_SELECT_RESIDUE + "WHERE book_id=?";
    private static final String SELECT_RESIDUE_BY_SELLER = QUERY_SELECT_RESIDUE + "WHERE seller_id=?";
    private static final String SELECT_RESIDUE_BY_SELLER_AND_BOOK = QUERY_SELECT_RESIDUE + "WHERE seller_id=? and book_id=?";
    private static final String SELECT_RESIDUE_BY_NULL_SELLER_AND_BOOK = QUERY_SELECT_RESIDUE + "WHERE seller_id IS NULL and book_id=?";
    private static final String QUERY_UPDATE_RESIDUE = "UPDATE Residue SET count_of_book=? WHERE residue_id=?";

    private static final String[] columnNames = new String[] {
            "residue_id",
            "count_of_book",
            "seller_id",
            "book_id",
            "is_store"
    };
//
//    private static final RowMapper<Residue> RESIDUE_ROW_MAPPER = new RowMapper<Residue>() {
//        @Override
//        public Residue mapRow(ResultSet rs, int rowNum) throws SQLException {
//            return new Residue(
//                    rs.getInt(columnNames[0]),
//                    rs.getInt(columnNames[1]),
//                    rs.getInt(columnNames[2]),
//                    rs.getInt(columnNames[3]),
//                    rs.getBoolean(columnNames[4])
//            );
//        }
//    };
//
    public ResidueDao(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }
//
//    @Override
//    public void save(Residue obj) {
//        try {
//            String query = "BEGIN INSERT INTO Residue (count_of_book,seller_id,book_id,is_store) values (?,?,?,?) returning residue_id into ?; END;";
//            CallableStatement cs = null;
//                cs = getConnection().prepareCall(query);
//            cs.setInt(columnNames[1], obj.getCountOfBook());
//            cs.setInt(columnNames[2], obj.getSellerId());
//            cs.setInt(columnNames[3], obj.getBookId());
//            cs.setBoolean(columnNames[4], obj.isStore());
//            cs.registerOutParameter(4, OracleTypes.NUMBER);
//            cs.execute();
//            System.out.println(cs.getInt(2));
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    public String getTableName() {
//        return TABLE_NAME;
//    }
//
//    @Override
//    public RowMapper<Residue> getRowMapper() {
//        return RESIDUE_ROW_MAPPER;
//    }
//
//    @Override
//    public Map<String, Object> getInsertMapParams(Residue obj) {
//        Map<String, Object> params = super.getInsertMapParams(obj);
//        params.put(columnNames[1], obj.getCountOfBook());
//        params.put(columnNames[2], obj.getSellerId());
//        params.put(columnNames[3], obj.getBookId());
//        params.put(columnNames[4], obj.isStore());
//        return params;
//    }
//
//    @Override
//    protected String getQueryUpdate() {
//        return QUERY_UPDATE_RESIDUE;
//    }
//
//    @Override
//    protected Object[] getUpdateParams(Residue obj) {
//        return new Object[]{
//                obj.getCountOfBook(),
//                obj.getId()
//        };
//    }
//
//    @Override
//    protected String getIdColumnName() {
//        return columnNames[0];
//    }
//
//    public List<Residue> getResiduesByBookId(Integer bookId) {
//        return getJdbcTemplate().query(
//                SELECT_RESIDUE_BY_BOOK,
//                new Object[]{bookId},
//                getRowMapper()
//        );
//    }
//
//    public List<Residue> getResiduesBySellerId(Integer sellerId) {
//        return getJdbcTemplate().query(
//                SELECT_RESIDUE_BY_SELLER,
//                new Object[]{sellerId},
//                getRowMapper()
//        );
//    }
//
//    public Residue getResiduesBySellerIdAndBookId(Integer sellerId, Integer bookId) {
//        try {
//            if (sellerId != null) {
//                return getJdbcTemplate().queryForObject(
//                                SELECT_RESIDUE_BY_SELLER_AND_BOOK,
//                        new Object[]{sellerId, bookId},
//                        getRowMapper()
//                );
//            } else {
//                return getJdbcTemplate().queryForObject(
//                                SELECT_RESIDUE_BY_NULL_SELLER_AND_BOOK,
//                        new Object[]{bookId},
//                        getRowMapper()
//                );
//            }
//
//        } catch (EmptyResultDataAccessException e) {
//            return null;
//        }
//    }
}
