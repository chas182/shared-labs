package bsu.zabavskaya.db.service;

import bsu.zabavskaya.db.dao.BookDao;
import bsu.zabavskaya.db.repo.BookRepository;
import bsu.zabavskaya.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    @Transactional(readOnly = true, propagation = Propagation.NESTED)
    public List<Book> getAll() {
        return bookRepository.findAll();
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void save(Book book) {
        bookRepository.save(book);
    }
}
