package bsu.zabavskaya.db.dao;

import bsu.zabavskaya.model.Book;
import bsu.zabavskaya.model.Waybill;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.io.UnsupportedEncodingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class WaybillDao extends AbstractDao<Waybill> {

    private static final String TABLE_NAME = "Waybill";
    private static final String QUERY_SELECT_ALL = "SELECT * FROM Waybill " +
            " LEFT JOIN books_per_transfer ON waybill.books_per_transfer_id=books_per_transfer.books_per_transfer_id" +
            " LEFT JOIN book ON book.book_id = books_per_transfer.book_id";
    private static final String QUERY_UPDATE_BOOK = "UPDATE Waybill SET waybill_number=?,data=?,type=?,books_per_transfer_id=? WHERE id_waybill=?";
        private static final String QUERY_SELECT_BETWEEN_DATE = "SELECT * FROM Waybill" +
                " LEFT JOIN books_per_transfer ON waybill.books_per_transfer_id=books_per_transfer.books_per_transfer_id" +
                " LEFT JOIN book ON book.book_id = books_per_transfer.book_id  WHERE (data BETWEEN '2014-11-01' and '2014-11-17') and type = 'продавец-склад'";
    private static final String[] columnNames = new String[] {
            "id_waybill",
            "waybill_number",
            "data",
            "type",
            "number_of_books",
            "book_id",
            "name",
            "author",
            "publishing",
            "selling_price",
            "purchase_price",
            "books_per_transfer_id"
    };
//
//    private static final RowMapper<Waybill> WAYBILL_ROW_MAPPER = new RowMapper<Waybill>() {
//        @Override
//        public Waybill mapRow(ResultSet rs, int rowNum) throws SQLException {
//            try {
//                return new Waybill(
//                        rs.getInt(columnNames[0]),
//                        rs.getInt(columnNames[1]),
//                        rs.getDate(columnNames[2]),
//                        Waybill.Type.fromString(new String(rs.getString(columnNames[3]).getBytes("cp1251"), "utf-8")),
//                        rs.getInt(columnNames[4]),
//                            new Book(
//                            rs.getInt(columnNames[5]),
//                            new String(rs.getString(columnNames[6]).getBytes("cp1251"), "utf-8"),
//                            rs.getString(columnNames[7]),
//                            rs.getString(columnNames[8]),
//                            rs.getInt(columnNames[9]),
//                            rs.getInt(columnNames[10]))
//                );
//            } catch (UnsupportedEncodingException e) {
//                return null;
//            }
//        }
//    };

    public WaybillDao(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

//    @Override
//    public void save(Waybill obj) {
//
//    }
//
//    @Override
//    public String getTableName() {
//        return TABLE_NAME;
//    }
//
//    @Override
//    public RowMapper<Waybill> getRowMapper() {
//        return WAYBILL_ROW_MAPPER;
//    }
//
//    @Override
//    public Map<String, Object> getInsertMapParams(Waybill obj) {
//        Map<String, Object> params = super.getInsertMapParams(obj);
//        params.put(columnNames[1], obj.getNumber());
//        params.put(columnNames[2], obj.getDate());
//        params.put(columnNames[3], obj.getType().getString());
//        params.put(columnNames[4], obj.getBookPerTransferId());
////        params.put(columnNames[5], obj.getBook().getId());
//        return params;
//    }
//
//    @Override
//    protected String getQueryUpdate() {
//        return QUERY_UPDATE_BOOK;
//    }
//
//    @Override
//    protected Object[] getUpdateParams(Waybill obj) {
//        return new Object[]{
//                obj.getNumber(),
//                obj.getDate(),
//                obj.getType().getString(),
//                obj.getBookPerTransferId(),
//                obj.getId()
//        };
//    }
//
//    @Override
//    protected String getIdColumnName() {
//        return columnNames[0];
//    }
//
//    public List<Waybill> getAll() {
//        return getJdbcTemplate().query(
//                QUERY_SELECT_ALL,
//                WAYBILL_ROW_MAPPER
//        );
//    }
//    public List<Waybill> getAllBeetwenDates() {
//        return getJdbcTemplate().query(
//                QUERY_SELECT_BETWEEN_DATE,
//                WAYBILL_ROW_MAPPER
//        );
//    }
}
