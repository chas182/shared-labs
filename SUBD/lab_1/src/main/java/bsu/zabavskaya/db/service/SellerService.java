package bsu.zabavskaya.db.service;

import bsu.zabavskaya.db.dao.SellerDao;
import bsu.zabavskaya.db.repo.SellerRepository;
import bsu.zabavskaya.model.Seller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SellerService {

    @Autowired
    private SellerRepository sellerRepository;

    @Transactional(readOnly = true, propagation = Propagation.NESTED)
    public List<Seller> getAll() {
        return sellerRepository.findAll();
    }
}
