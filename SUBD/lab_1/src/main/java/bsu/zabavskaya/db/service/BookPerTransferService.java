package bsu.zabavskaya.db.service;

import bsu.zabavskaya.db.repo.BookPerTransferRepository;
import bsu.zabavskaya.model.BookPerTransfer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class BookPerTransferService {

    @Autowired
    private BookPerTransferRepository bookPerTransferRepository;

    @Transactional(readOnly = true, propagation = Propagation.NESTED)
    public List<BookPerTransfer> getAll() {
        return bookPerTransferRepository.findAll();
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void save(BookPerTransfer bookPerTransfer) {
        bookPerTransferRepository.save(bookPerTransfer);
    }

}
