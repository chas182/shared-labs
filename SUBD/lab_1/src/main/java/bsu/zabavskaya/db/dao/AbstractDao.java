package bsu.zabavskaya.db.dao;

import bsu.zabavskaya.model.BaseModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractDao<T extends BaseModel> extends CustomJdbcDaoSupport {

    private static final Logger logger = LoggerFactory.getLogger(AbstractDao.class);

    private static final String QUERY_SELECT = "SELECT * FROM ";

    private static final String QUERY_DELETE = "DELETE FROM ";

    private static final String WHERE_ID = " WHERE ";

    public AbstractDao(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
//        getJdbcInsert().setTableName(getTableName());
//        getJdbcInsert().setGeneratedKeyName(getGeneratedKeyColumn());
    }
//
//    public T getById(Integer id) {
//        try {
//            return getJdbcTemplate().queryForObject(
//                    getQuerySelect(),
//                    new Object[]{id},
//                    getRowMapper()
//            );
//        } catch (EmptyResultDataAccessException e) {
//            return null;
//        }
//    }
//
//    public abstract void save(T obj);
//
//    public void update(T obj) {
//        getJdbcTemplate().update(
//                getQueryUpdate(),
//                getUpdateParams(obj)
//        );
//    }
//
//    public void delete(Integer id) {
//        getJdbcTemplate().update(
//                getQueryDelete(),
//                id
//        );
//    }
//
//    public abstract String getTableName();
//
//    public String getGeneratedKeyColumn() {
//        return "id";
//    }
//
//    public abstract RowMapper<T> getRowMapper();
//
//    public Map<String, Object> getInsertMapParams(T obj) {
//        Map<String, Object> params = new HashMap<String, Object>();
////        params.put(getIdColumnName(), obj.getId());
//        return params;
//    }
//
//    private String getQuerySelect() {
//        return new StringBuilder(QUERY_SELECT)
//                .append(getTableName())
//                .append(WHERE_ID)
//                .append(getIdColumnName())
//                .append("=?")
//                .toString();
//    }
//
//    protected abstract String getQueryUpdate();
//
//    protected abstract Object[] getUpdateParams(T obj);
//
//    private String getQueryDelete() {
//        return new StringBuilder(QUERY_DELETE)
//                .append(getTableName())
//                .append(WHERE_ID)
//                .append(getIdColumnName())
//                .append("=?")
//                .toString();
//
//    }
//
//    protected abstract String getIdColumnName();
}
