package bsu.zabavskaya.db.repo;

import bsu.zabavskaya.model.Seller;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @author Vlad Abramov
 * @since 5/25/2015
 */
public interface SellerRepository extends CrudRepository<Seller, Integer> {

    List<Seller> findAll();
}
