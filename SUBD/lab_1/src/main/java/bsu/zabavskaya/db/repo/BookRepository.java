package bsu.zabavskaya.db.repo;

import bsu.zabavskaya.model.Book;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @author Vlad Abramov
 * @since 5/25/2015
 */
public interface BookRepository extends CrudRepository<Book, Integer> {

    List<Book> findAll();
}
