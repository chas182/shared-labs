package bsu.zabavskaya.db.repo;

import bsu.zabavskaya.model.Waybill;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

/**
 * @author Vlad Abramov
 * @since 5/25/2015
 */
public interface WaybillRepository extends CrudRepository<Waybill, Integer> {

    List<Waybill> findAll();
    List<Waybill> findByDateBetweenAndType(Date from, Date to, Waybill.Type type);
}
