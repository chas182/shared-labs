package bsu.zabavskaya;


import bsu.zabavskaya.ui.MainForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;

public class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        ContextHolder.init();

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ignored) {}

        EventQueue.invokeLater(new Runnable() {
            public void run() {
                logger.info("Invoke main form");
                try {
                    new MainForm().setVisible(true);
                } catch (Exception e) {
                    logger.error("Error",e);
                    System.exit(-1);
                }
            }
        });
    }
}
