package bsu.zabavskaya.model;


public abstract class BaseModel {

    public abstract Integer getId();

    public abstract void setId(Integer id);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseModel baseModel = (BaseModel) o;

        return getId().equals(baseModel.getId());

    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

}
