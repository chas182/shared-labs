package bsu.zabavskaya.model;

import javax.persistence.*;

@Entity(name = "SELLER")
public class Seller extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "SELLER_ID")
    private Integer sellerId;

    @Column(name = "NAME")
    private String name;

    @Column(name = "ADDRESS")
    private String address;

    @Column(name = "PERCENTAGE_OF_REALIZATION")
    private Integer percentageOfRealization;

    public Seller() {
    }

    public Seller(String name, String address, Integer percentageOfRealization) {
        this.name = name;
        this.address = address;
        this.percentageOfRealization = percentageOfRealization;
    }

    public Integer getId() {
        return sellerId;
    }

    public void setId(Integer id) {
        this.sellerId = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getPercentageOfRealization() {
        return percentageOfRealization;
    }

    public void setPercentageOfRealization(Integer percentageOfRealization) {
        this.percentageOfRealization = percentageOfRealization;
    }

    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (percentageOfRealization != null ? percentageOfRealization.hashCode() : 0);
        return result;
    }
}
