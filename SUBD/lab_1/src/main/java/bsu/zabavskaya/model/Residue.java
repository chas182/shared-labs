package bsu.zabavskaya.model;

import javax.persistence.*;

@Entity(name = "RESIDUE")
public class Residue extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "RESIDUE_ID")
    private Integer residueId;
    @Column(name = "COUNT_OF_BOOK")
    private Integer countOfBook;
    @ManyToOne
    @JoinColumn(name = "SELLER_ID", referencedColumnName = "SELLER_ID")
    private Seller seller;
    @ManyToOne
    @JoinColumn(name = "BOOK_ID", referencedColumnName = "BOOK_ID")
    private Book book;
    @Column(name = "IS_STORE")
    private boolean isStore;

    public Residue() {
    }

    public Residue(Integer countOfBook, Seller seller, Book book, boolean isStore) {
        this.countOfBook = countOfBook;
        this.seller = seller;
        this.book = book;
        this.isStore = isStore;
    }

    @Override
    public Integer getId() {
        return residueId;
    }

    @Override
    public void setId(Integer id) {
        this.residueId = id;
    }

    public Integer getCountOfBook() {
        return countOfBook;
    }

    public void setCountOfBook(Integer countOfBook) {
        this.countOfBook = countOfBook;
    }

    public boolean isStore() {
        return isStore;
    }

    public void setStore(boolean isStore) {
        this.isStore = isStore;
    }

    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (countOfBook != null ? countOfBook.hashCode() : 0);
        result = 31 * result + (seller != null ? seller.hashCode() : 0);
        result = 31 * result + (book != null ? book.hashCode() : 0);
        result = 31 * result + (isStore ? 1 : 0);
        return result;
    }
}
