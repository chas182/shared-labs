package bsu.zabavskaya.model;

import javax.persistence.*;

@Entity(name = "BOOKS_PER_TRANSFER")
public class BookPerTransfer extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "BOOKS_PER_TRANSFER_ID")
    private Integer bookPerTransferId;
    @Column(name = "NUMBER_OF_BOOKS")
    private Integer numberOfBooks;
    @OneToOne
    @JoinColumn(name = "BOOK_ID", referencedColumnName = "BOOK_ID")
    private Book book;

    public BookPerTransfer() {
    }

    public BookPerTransfer(Integer numberOfBooks, Book book) {
        this.numberOfBooks = numberOfBooks;
        this.book = book;
    }

    @Override
    public Integer getId() {
        return bookPerTransferId;
    }

    @Override
    public void setId(Integer id) {
        this.bookPerTransferId = id;
    }

    public Integer getNumberOfBooks() {
        return numberOfBooks;
    }

    public void setNumberOfBooks(Integer numberOfBooks) {
        this.numberOfBooks = numberOfBooks;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (numberOfBooks != null ? numberOfBooks.hashCode() : 0);
        result = 31 * result + (book != null ? book.hashCode() : 0);
        return result;
    }
}
