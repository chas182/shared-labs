package bsu.zabavskaya.model;

public class SellerMetadata {
    private Integer countOfBook;
    private Book book;

    public SellerMetadata(Integer countOfBook, Book book) {
        this.countOfBook = countOfBook;
        this.book = book;
    }

    public Integer getCountOfBook() {
        return countOfBook;
    }

    public Book getBook() {
        return book;
    }
}
