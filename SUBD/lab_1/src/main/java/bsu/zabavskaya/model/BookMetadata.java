package bsu.zabavskaya.model;

public class BookMetadata {
    private Integer countOfBook;
    private Seller seller;

    public BookMetadata(Integer countOfBook, Seller seller) {
        this.countOfBook = countOfBook;
        this.seller = seller;
    }

    public Integer getCountOfBook() {
        return countOfBook;
    }

    public Seller getSeller() {
        return seller;
    }
}
