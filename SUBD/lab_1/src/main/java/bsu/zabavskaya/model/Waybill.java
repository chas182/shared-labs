package bsu.zabavskaya.model;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "WAYBILL")
public class Waybill extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID_WAYBILL")
    private Integer operationId;
    @Column(name = "WAYBILL_NUMBER")
    private Integer number;
    @Column(name = "DATA")
    private Date date;
    @Column(name = "TYPE")
    private Type type;
    @OneToOne
    @JoinColumn(name = "BOOKS_PER_TRANSFER_ID", referencedColumnName = "BOOKS_PER_TRANSFER_ID")
    private BookPerTransfer bookPerTransfer;

    public Waybill() {
    }

    public Waybill(Integer number, Date date, Type type, BookPerTransfer bookPerTransfer) {
        this.number = number;
        this.date = date;
        this.type = type;
        this.bookPerTransfer = bookPerTransfer;
    }

    @Override
    public Integer getId() {
        return operationId;
    }

    @Override
    public void setId(Integer id) {
        this.operationId = id;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public BookPerTransfer getBookPerTransfer() {
        return bookPerTransfer;
    }

    public void setBookPerTransfer(BookPerTransfer bookPerTransfer) {
        this.bookPerTransfer = bookPerTransfer;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (number != null ? number.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (bookPerTransfer != null ? bookPerTransfer.hashCode() : 0);
        return result;
    }

    public enum Type {
        STORE_SELLER("склад-продавец"),
        SEllER_STORE("продавец-склад"),
        SELLER_SELLER("продавец-продавец");

        private final String type;

        Type(String type) {
            this.type = type;
        }

        public static Type fromString(String str) {
            if (SELLER_SELLER.type.equals(str)) {
                return SELLER_SELLER;
            } else if (STORE_SELLER.type.equals(str)) {
                return STORE_SELLER;
            } else if (SEllER_STORE.type.equals(str)) {
                return SEllER_STORE;
            }
            return null;
        }

        public String getString() {
            return type;
        }
    }
}
