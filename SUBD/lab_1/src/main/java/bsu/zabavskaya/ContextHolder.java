package bsu.zabavskaya;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Vlad Abramov
 * @since 15.11.2014
 */
public final class ContextHolder {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    private static ApplicationContext applicationContext = null;

    public static ApplicationContext getAppContext() {
        if (applicationContext == null) {
            init();
        }
        return applicationContext;
    }

    public static void init() {
        if (applicationContext == null) {
            logger.info("Initializing Spring context.");
            long start = System.currentTimeMillis();

            applicationContext = new ClassPathXmlApplicationContext("/application-context.xml");
            logger.info("Spring context initialized in {} ms.", System.currentTimeMillis() - start);
        }
    }

    public static Object getBean(String beanName) {
        return applicationContext.getBean(beanName);
    }

}
