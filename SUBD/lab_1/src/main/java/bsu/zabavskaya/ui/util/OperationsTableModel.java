package bsu.zabavskaya.ui.util;

import bsu.zabavskaya.model.Waybill;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import java.text.SimpleDateFormat;
import java.util.List;

public class OperationsTableModel extends DefaultTableModel {

    private static final long serialVersionUID = 978751825923835112L;

    public OperationsTableModel(List<Waybill> waybills) {
        String[] columnNames = new String[]{"№ операции", "Дата", "Тип", "Книга", "Кол-во"};
        Object[][] data = new Object[waybills.size()][columnNames.length];
        for (int i = 0; i < waybills.size(); ++i) {
            Waybill waybill = waybills.get(i);
            data[i][0] = waybill.getNumber();
            data[i][1] = new SimpleDateFormat("yyyy-MM-dd").format(waybill.getDate());
            data[i][2] = waybill.getType().getString();
            data[i][3] = waybill.getBookPerTransfer().getBook().getName();
            data[i][4] = waybill.getBookPerTransfer().getNumberOfBooks();
        }
        setDataVector(data, columnNames);
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

}
