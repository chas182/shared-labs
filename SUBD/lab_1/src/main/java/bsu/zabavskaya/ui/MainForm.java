package bsu.zabavskaya.ui;

import bsu.zabavskaya.ContextHolder;
import bsu.zabavskaya.db.service.*;
import bsu.zabavskaya.model.*;
import bsu.zabavskaya.ui.util.MetadataTableModel;
import bsu.zabavskaya.ui.util.OperationsTableModel;
import java.awt.CardLayout;
import java.util.Date;
import java.util.List;
import java.util.Random;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("unchecked")
public class MainForm extends javax.swing.JFrame {

    private static final Logger logger = LoggerFactory.getLogger(MainForm.class);
    private static final String BOOKS_CARD = "BOOKS_CARD";
    private static final String SELLER_CARD = "SELLER_CARD";
    private static final String OPERATIONS_CARD = "OPERATIONS_CARD";

    private BookService bookService;
    private SellerService sellerService;
    private ResidueService residueService;
    private WaybillService waybillService;
    private BookPerTransferService bookPerTransferService;
    private StubInit stubInit;
    private java.util.List<Book> cachedBooks;
    private java.util.List<Seller> cachedSellers;

    private SellerSendDialog sellerDialog = new SellerSendDialog(this, true);
    private BookDialog bookDialog = new BookDialog(this, true);

    public MainForm() {
        bookService = (BookService) ContextHolder.getBean("bookService");
        sellerService = (SellerService) ContextHolder.getBean("sellerService");
        residueService = (ResidueService) ContextHolder.getBean("residueService");
        waybillService = (WaybillService) ContextHolder.getBean("waybillService");
        stubInit = (StubInit) ContextHolder.getBean("stubInit");
        bookPerTransferService = (BookPerTransferService) ContextHolder.getBean("bookPerTransferService");
        if (bookService.getAll().size() == 0)
            stubInit.init();
        initComponents();
        resetBookList();
        showCard(BOOKS_CARD);
    }

    private void showCard(String card) {
        CardLayout cl = (CardLayout) (getContentPane().getLayout());
        cl.show(getContentPane(), card);

        switch (card) {
            case BOOKS_CARD:
                resetBookList();
                bookMI.setSelected(true);
                sellerMI.setSelected(false);
                operationsMI.setSelected(false);
                break;
            case SELLER_CARD:
                resetSellerList();
                bookMI.setSelected(false);
                sellerMI.setSelected(true);
                operationsMI.setSelected(false);
                break;
            case OPERATIONS_CARD:
                resetOperationsTable();
                bookMI.setSelected(false);
                sellerMI.setSelected(false);
                operationsMI.setSelected(true);
                break;
        }
    }

    private void resetBookList(int index) {
        resetBookList();
        booksJList.setSelectedIndex(index);
    }

    private void resetBookList() {
        DefaultListModel<String> model = new DefaultListModel<String>();

        cachedBooks = bookService.getAll();
        for (Book book : cachedBooks) {
            model.addElement(book.getName());
        }
        booksJList.setModel(model);
        booksJList.setSelectedIndex(0);
    }

    private void resetSellerList() {
        DefaultListModel<String> model = new DefaultListModel<String>();

        cachedSellers = sellerService.getAll();
        for (Seller seller : cachedSellers) {
            model.addElement(seller.getName());
        }
        sellerJList.setModel(model);
        sellerJList.setSelectedIndex(0);
    }

    private void resetOperationsTable() {
        operationsTable.setModel(new OperationsTableModel(waybillService.getAll()));
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        sellerLayout = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        sellerJList = new javax.swing.JList();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        sellerNameLabel = new javax.swing.JLabel();
        sellerAdressLabel = new javax.swing.JLabel();
        sellerPercentageOfRealizationLabel = new javax.swing.JLabel();
        resetSellerList = new javax.swing.JButton();
        jScrollPane5 = new javax.swing.JScrollPane();
        sellerMetadataTable = new javax.swing.JTable();
        bookLayout = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        bookMetadataTable = new javax.swing.JTable();
        jScrollPane1 = new javax.swing.JScrollPane();
        booksJList = new javax.swing.JList();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        bookNameLabel = new javax.swing.JLabel();
        bookAuthorLabel = new javax.swing.JLabel();
        bookPublishingLabel = new javax.swing.JLabel();
        bookPriceLabel = new javax.swing.JLabel();
        resetBookList = new javax.swing.JButton();
        sendToSellerButton = new javax.swing.JButton();
        sendToStoreButton = new javax.swing.JButton();
        takeFromStoreButton = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        operationsLayout = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        operationsTable = new javax.swing.JTable();
        jMenuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        bookMI = new javax.swing.JCheckBoxMenuItem();
        sellerMI = new javax.swing.JCheckBoxMenuItem();
        operationsMI = new javax.swing.JCheckBoxMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        exitMI = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Субд лаба");
        setLocationByPlatform(true);
        setPreferredSize(new java.awt.Dimension(600, 500));
        setResizable(false);
        getContentPane().setLayout(new java.awt.CardLayout());

        sellerJList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        sellerJList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                sellerJListValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(sellerJList);

        jLabel5.setText("Процент реализации:");

        jLabel6.setText("Адрес:");

        jLabel7.setText("Имя, фамилия:");

        resetSellerList.setText("Обновить список");
        resetSellerList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetSellerListActionPerformed(evt);
            }
        });

        sellerMetadataTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane5.setViewportView(sellerMetadataTable);

        javax.swing.GroupLayout sellerLayoutLayout = new javax.swing.GroupLayout(sellerLayout);
        sellerLayout.setLayout(sellerLayoutLayout);
        sellerLayoutLayout.setHorizontalGroup(
            sellerLayoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sellerLayoutLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(sellerLayoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(resetSellerList, javax.swing.GroupLayout.DEFAULT_SIZE, 171, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGroup(sellerLayoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(sellerLayoutLayout.createSequentialGroup()
                        .addGroup(sellerLayoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel5)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(sellerLayoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(sellerAdressLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)
                            .addComponent(sellerNameLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(sellerPercentageOfRealizationLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(sellerLayoutLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        sellerLayoutLayout.setVerticalGroup(
            sellerLayoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sellerLayoutLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(sellerLayoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, sellerLayoutLayout.createSequentialGroup()
                        .addGroup(sellerLayoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(sellerNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(sellerLayoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(sellerAdressLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(sellerLayoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(sellerPercentageOfRealizationLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 298, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(resetSellerList)
                .addContainerGap(67, Short.MAX_VALUE))
        );

        getContentPane().add(sellerLayout, "SELLER_CARD");

        bookMetadataTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane4.setViewportView(bookMetadataTable);

        booksJList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        booksJList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                booksJListValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(booksJList);

        jLabel3.setText("Издательство:");

        jLabel2.setText("Автор:");

        jLabel1.setText("Название:");

        jLabel4.setText("Цена:");

        resetBookList.setText("Обновить список");
        resetBookList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetBookListActionPerformed(evt);
            }
        });

        sendToSellerButton.setText("Передать продавцу");
        sendToSellerButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendToSeller(evt);
            }
        });

        sendToStoreButton.setText("Передать на склад");
        sendToStoreButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendToStore(evt);
            }
        });

        takeFromStoreButton.setText("Взять со склада");
        takeFromStoreButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                takeFromStore(evt);
            }
        });

        jButton1.setText("Просмотреть возвраты");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Добавить книгу");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addBook(evt);
            }
        });

        jButton3.setText("Редакрировать книгу");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editBook(evt);
            }
        });

        jButton4.setText("Удалить книгу");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteBook(evt);
            }
        });

        javax.swing.GroupLayout bookLayoutLayout = new javax.swing.GroupLayout(bookLayout);
        bookLayout.setLayout(bookLayoutLayout);
        bookLayoutLayout.setHorizontalGroup(
            bookLayoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bookLayoutLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(bookLayoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bookLayoutLayout.createSequentialGroup()
                        .addGroup(bookLayoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(resetBookList, javax.swing.GroupLayout.DEFAULT_SIZE, 171, Short.MAX_VALUE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, bookLayoutLayout.createSequentialGroup()
                                .addComponent(jButton1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                        .addGroup(bookLayoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(bookLayoutLayout.createSequentialGroup()
                                .addGroup(bookLayoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel4))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(bookLayoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(bookNameLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(bookAuthorLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(bookPublishingLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(bookPriceLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addGroup(bookLayoutLayout.createSequentialGroup()
                                .addGroup(bookLayoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(takeFromStoreButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(sendToSellerButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(sendToStoreButton)
                                .addGap(0, 41, Short.MAX_VALUE))))
                    .addGroup(bookLayoutLayout.createSequentialGroup()
                        .addComponent(jButton2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton4)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        bookLayoutLayout.setVerticalGroup(
            bookLayoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bookLayoutLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(bookLayoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(bookLayoutLayout.createSequentialGroup()
                        .addGroup(bookLayoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(bookNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(bookLayoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(bookAuthorLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(bookLayoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(bookPublishingLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(bookLayoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(bookPriceLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 298, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(bookLayoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(resetBookList)
                    .addComponent(sendToSellerButton)
                    .addComponent(sendToStoreButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(bookLayoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(takeFromStoreButton)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(bookLayoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2)
                    .addComponent(jButton3)
                    .addComponent(jButton4))
                .addGap(0, 3, Short.MAX_VALUE))
        );

        getContentPane().add(bookLayout, "BOOKS_CARD");

        operationsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane3.setViewportView(operationsTable);

        javax.swing.GroupLayout operationsLayoutLayout = new javax.swing.GroupLayout(operationsLayout);
        operationsLayout.setLayout(operationsLayoutLayout);
        operationsLayoutLayout.setHorizontalGroup(
            operationsLayoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 504, Short.MAX_VALUE)
        );
        operationsLayoutLayout.setVerticalGroup(
            operationsLayoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 409, Short.MAX_VALUE)
        );

        getContentPane().add(operationsLayout, "OPERATIONS_CARD");

        fileMenu.setText("Экран");

        bookMI.setText("Книги");
        bookMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bookMIActionPerformed(evt);
            }
        });
        fileMenu.add(bookMI);

        sellerMI.setText("Продавцы");
        sellerMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sellerMIActionPerformed(evt);
            }
        });
        fileMenu.add(sellerMI);

        operationsMI.setText("Операции");
        operationsMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                operationsMIActionPerformed(evt);
            }
        });
        fileMenu.add(operationsMI);
        fileMenu.add(jSeparator1);

        exitMI.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, java.awt.event.InputEvent.ALT_MASK));
        exitMI.setText("Выход");
        exitMI.setToolTipText("");
        exitMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitMIActionPerformed(evt);
            }
        });
        fileMenu.add(exitMI);

        jMenuBar.add(fileMenu);

        setJMenuBar(jMenuBar);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exitMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMIActionPerformed
        System.exit(0);
    }//GEN-LAST:event_exitMIActionPerformed

    private void resetBookListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetBookListActionPerformed
        logger.debug("Reset book list");
        resetBookList();
    }//GEN-LAST:event_resetBookListActionPerformed

    private void booksJListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_booksJListValueChanged
        int index = booksJList.getSelectedIndex();
        if (index != -1) {
            Book curBook = cachedBooks.get(index);
            bookNameLabel.setText(curBook.getName());
            bookAuthorLabel.setText(curBook.getAuthor());
            bookPublishingLabel.setText(curBook.getPublishing());
            bookPriceLabel.setText(curBook.getSellingPrice().toString());
            List<BookMetadata> booksMetadata = residueService.getBookMetadata(curBook);
            bookMetadataTable.setModel(new MetadataTableModel(booksMetadata, BookMetadata.class));
            boolean onStore = false;
            boolean isSeller = false;
            for (BookMetadata bookMetadata : booksMetadata) {
                if (bookMetadata.getSeller() == null) {
                    onStore = true;
                    break;
                }
            }
            for (BookMetadata bookMetadata : booksMetadata) {
                if (bookMetadata.getSeller() != null) {
                    isSeller = true;
                    break;
                }
            }
            takeFromStoreButton.setEnabled(onStore);
            sendToSellerButton.setEnabled(isSeller);
            sendToStoreButton.setEnabled(isSeller);
        }
    }//GEN-LAST:event_booksJListValueChanged

    private void bookMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bookMIActionPerformed
        showCard(BOOKS_CARD);
    }//GEN-LAST:event_bookMIActionPerformed

    private void sellerMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sellerMIActionPerformed
        showCard(SELLER_CARD);
    }//GEN-LAST:event_sellerMIActionPerformed

    private void sellerJListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_sellerJListValueChanged
        int index = sellerJList.getSelectedIndex();
        if (index != -1) {
            Seller curSeller = cachedSellers.get(index);
            sellerNameLabel.setText(curSeller.getName());
            sellerAdressLabel.setText(curSeller.getAddress());
            sellerPercentageOfRealizationLabel.setText(curSeller.getPercentageOfRealization().toString() + " %");
            List<SellerMetadata> sellersMetadata = residueService.getSellerMetadata(curSeller);
            sellerMetadataTable.setModel(new MetadataTableModel(sellersMetadata, SellerMetadata.class));
        }
    }//GEN-LAST:event_sellerJListValueChanged

    private void resetSellerListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetSellerListActionPerformed
        resetSellerList();
    }//GEN-LAST:event_resetSellerListActionPerformed

    private void takeFromStore(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_takeFromStore
        Book curBook = cachedBooks.get(booksJList.getSelectedIndex());
        List<BookMetadata> booksMetadata = residueService.getBookMetadata(curBook);
        BookMetadata bookMetadata = null;
        for (BookMetadata metadata : booksMetadata) {
            if (metadata.getSeller() == null) {
                bookMetadata = metadata;
            }
        }
        sellerDialog.setVisible(bookMetadata.getCountOfBook());

        if (sellerDialog.getSelectedSeller() != null) {
            Seller sellerReceiver = sellerDialog.getSelectedSeller();
            residueService.doTransferOperation(null, sellerReceiver, curBook, sellerDialog.getCountOfBook());

            BookPerTransfer bookPerTransfer = new BookPerTransfer(sellerDialog.getCountOfBook(), curBook);
            bookPerTransferService.save(bookPerTransfer);
            Waybill waybill = new Waybill(0,new Date(), Waybill.Type.STORE_SELLER, bookPerTransfer);
            waybillService.save(waybill);

            resetBookList(booksJList.getSelectedIndex());
        }
    }//GEN-LAST:event_takeFromStore

    private void sendToStore(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendToStore
        Book curBook = cachedBooks.get(booksJList.getSelectedIndex());
        List<BookMetadata> booksMetadata = residueService.getBookMetadata(curBook);
        BookMetadata bookMetadata = null;
        if (bookMetadataTable.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(this, "Выберите пожалуйста продавца-отправителя из таблицы.");
            return;
        } else {
            bookMetadata = booksMetadata.get(bookMetadataTable.getSelectedRow());
            if (bookMetadata.getSeller() == null) {
                JOptionPane.showMessageDialog(this, "Выберите пожалуйста продавца-отправителя из таблицы.");
                return;
            }
        }
        bookMetadata = booksMetadata.get(bookMetadataTable.getSelectedRow());
        String countStr = JOptionPane.showInputDialog(this, "Введите кол-во книжек от 0 до " + bookMetadata.getCountOfBook());

        int count = 0;
        try {
            count = Integer.parseInt(countStr);
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Введите число");
            return;
        }
        if (count > bookMetadata.getCountOfBook() || count <= 0) {
            JOptionPane.showMessageDialog(this, "Число должно быть от 0 до " + bookMetadata.getCountOfBook());
            return;
        } else {
            Seller sellerSender = bookMetadata.getSeller();
            residueService.doTransferOperation(sellerSender, null, curBook, count);

            BookPerTransfer bookPerTransfer = new BookPerTransfer(count, curBook);
            bookPerTransferService.save(bookPerTransfer);
            Waybill waybill = new Waybill(0,new Date(), Waybill.Type.SEllER_STORE, bookPerTransfer);
            waybillService.save(waybill);

            resetBookList(booksJList.getSelectedIndex());
        }
    }//GEN-LAST:event_sendToStore

    private void sendToSeller(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendToSeller
        if (bookMetadataTable.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(this, "Выберите пожалуйста продавца-отправителя из таблицы.");
            return;
        }
        Book curBook = cachedBooks.get(booksJList.getSelectedIndex());
        List<BookMetadata> booksMetadata = residueService.getBookMetadata(curBook);
        BookMetadata bookMetadata = booksMetadata.get(bookMetadataTable.getSelectedRow());
        sellerDialog.setVisible(bookMetadata.getCountOfBook());

        if (sellerDialog.getSelectedSeller() != null) {
            Seller sellerSender = bookMetadata.getSeller();
            Seller sellerReceiver = sellerDialog.getSelectedSeller();
            residueService.doTransferOperation(sellerSender, sellerReceiver, curBook, sellerDialog.getCountOfBook());

            BookPerTransfer bookPerTransfer = new BookPerTransfer(sellerDialog.getCountOfBook(), curBook);
            bookPerTransferService.save(bookPerTransfer);
            Waybill waybill = new Waybill(0,new Date(), Waybill.Type.SELLER_SELLER, bookPerTransfer);
            waybillService.save(waybill);

            resetBookList(booksJList.getSelectedIndex());
        }
    }//GEN-LAST:event_sendToSeller

    private void operationsMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_operationsMIActionPerformed
        showCard(OPERATIONS_CARD);
    }//GEN-LAST:event_operationsMIActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        Book book = cachedBooks.get(booksJList.getSelectedIndex());
        List<Waybill> returntBooks = waybillService.getAllBetweenDates();
        DefaultListModel<String> model = new DefaultListModel<String>();

        cachedBooks.clear();
        for(Waybill waybill : returntBooks){
            cachedBooks.add(waybill.getBookPerTransfer().getBook());
            System.out.println("Название книги: "+waybill.getBookPerTransfer().getBook().getName()+
                    " кол-во " + waybill.getBookPerTransfer().getNumberOfBooks() + " дата возврата: " +waybill.getDate());
        }

        cachedBooks = bookService.getAll();
        for (Book book1 : cachedBooks) {
            model.addElement(book1.getName());
        }
        booksJList.setModel(model);
        booksJList.setSelectedIndex(0);

    }//GEN-LAST:event_jButton1ActionPerformed

    private void addBook(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addBook
        bookDialog.addBook();
        if (!bookDialog.isCancelled()) {
            Book book = bookDialog.getCurBook();
            bookService.save(book);
            residueService.save(new Residue(
                    bookDialog.getCountOfBook(),
                    null, book, true));
        }
        resetBookList();
    }//GEN-LAST:event_addBook

    private void editBook(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editBook
        if (booksJList.getSelectedIndex() == -1) {
            JOptionPane.showMessageDialog(this, "Выберите книгу.");
            return;
        }
        Book curBook = cachedBooks.get(booksJList.getSelectedIndex());
        if (curBook == null)
            return;

        bookDialog.editBook(curBook);
        if (!bookDialog.isCancelled()) {
            bookService.save(bookDialog.getCurBook());
        }
        resetBookList();
    }//GEN-LAST:event_editBook

    private void deleteBook(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteBook
        if (booksJList.getSelectedIndex() == -1) {
            JOptionPane.showMessageDialog(this, "Выберите книгу.");
            return;
        }
        Book curBook = cachedBooks.get(booksJList.getSelectedIndex());
        // TODO delete your book here
        resetBookList();
    }//GEN-LAST:event_deleteBook

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel bookAuthorLabel;
    private javax.swing.JPanel bookLayout;
    private javax.swing.JCheckBoxMenuItem bookMI;
    private javax.swing.JTable bookMetadataTable;
    private javax.swing.JLabel bookNameLabel;
    private javax.swing.JLabel bookPriceLabel;
    private javax.swing.JLabel bookPublishingLabel;
    private javax.swing.JList booksJList;
    private javax.swing.JMenuItem exitMI;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JMenuBar jMenuBar;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPanel operationsLayout;
    private javax.swing.JCheckBoxMenuItem operationsMI;
    private javax.swing.JTable operationsTable;
    private javax.swing.JButton resetBookList;
    private javax.swing.JButton resetSellerList;
    private javax.swing.JLabel sellerAdressLabel;
    private javax.swing.JList sellerJList;
    private javax.swing.JPanel sellerLayout;
    private javax.swing.JCheckBoxMenuItem sellerMI;
    private javax.swing.JTable sellerMetadataTable;
    private javax.swing.JLabel sellerNameLabel;
    private javax.swing.JLabel sellerPercentageOfRealizationLabel;
    private javax.swing.JButton sendToSellerButton;
    private javax.swing.JButton sendToStoreButton;
    private javax.swing.JButton takeFromStoreButton;
    // End of variables declaration//GEN-END:variables
}
