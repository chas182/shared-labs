package bsu.zabavskaya.ui;

import bsu.zabavskaya.ContextHolder;
import bsu.zabavskaya.db.service.SellerService;
import bsu.zabavskaya.model.Seller;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

public class SellerSendDialog extends javax.swing.JDialog {

    private List<Seller> cachedSellers;
    private SellerService sellerService;
    private Seller selectedSeller;
    private Integer countOfBook;

    public SellerSendDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                selectedSeller = null;
                setVisible(false);
            }
        });

        sellerService = (SellerService) ContextHolder.getBean("sellerService");

        resetSellerList();
    }

    private void resetSellerList() {
        DefaultListModel<String> model = new DefaultListModel<>();

        cachedSellers = sellerService.getAll();
        for (Seller seller : cachedSellers) {
            model.addElement(seller.getName());
        }
        sellersJList.setModel(model);
        sellersJList.setSelectedIndex(0);
    }

    public void setVisible(Integer countOfBook) {
        this.countOfBook = countOfBook;
        countOfBooksTF.setText("");
        resetSellerList();
        setVisible(true);
    }

    public Integer getCountOfBook() {
        return countOfBook;
    }

    public Seller getSelectedSeller() {
        return selectedSeller;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        sellersJList = new javax.swing.JList();
        jLabel1 = new javax.swing.JLabel();
        countOfBooksTF = new javax.swing.JTextField();
        sendButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Выберите получателя");
        setLocationByPlatform(true);

        sellersJList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        sellersJList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                sellersJListValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(sellersJList);

        jLabel1.setText("Кол-во:");

        sendButton.setText("Отправить");
        sendButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(sendButton, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(countOfBooksTF)))
                .addContainerGap())
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(countOfBooksTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sendButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void sendButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendButtonActionPerformed
        Integer count = 0;
        if (sellersJList.getSelectedIndex() == -1) {
            JOptionPane.showMessageDialog(this, "Выберите продавца");
        }
        try {
            count = Integer.parseInt(countOfBooksTF.getText());
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Введите число");
            return;
        }
        if (count > countOfBook || count <= 0) {
            JOptionPane.showMessageDialog(this, "Число должно быть от 0 до " + countOfBook);
            return;
        } else {
            countOfBook = count;
            selectedSeller = cachedSellers.get(sellersJList.getSelectedIndex());
        }
        setVisible(false);
    }//GEN-LAST:event_sendButtonActionPerformed

    private void sellersJListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_sellersJListValueChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_sellersJListValueChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField countOfBooksTF;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JList sellersJList;
    private javax.swing.JButton sendButton;
    // End of variables declaration//GEN-END:variables
}
