package bsu.zabavskaya.ui.util;

import bsu.zabavskaya.model.BookMetadata;
import bsu.zabavskaya.model.SellerMetadata;

import javax.swing.table.DefaultTableModel;
import java.util.List;

public class MetadataTableModel extends DefaultTableModel {
    private static final long serialVersionUID = 4015226686641412239L;

    public MetadataTableModel(List booksMetadata, Class clazz) {
        String secondColumn = "";
        if (clazz.equals(BookMetadata.class)) {
            secondColumn = "В наличии";
        } else if (clazz.equals(BookMetadata.class)) {
            secondColumn = "Книга";
        }
        String[] columnNames = new String[]{"Кол-во", secondColumn};
        Object[][] data = new Object[booksMetadata.size()][columnNames.length];
        for (int i = 0; i < booksMetadata.size(); ++i) {
            if (clazz.equals(BookMetadata.class)) {
                BookMetadata bookMetadata = (BookMetadata) booksMetadata.get(i);
                data[i][0] = bookMetadata.getCountOfBook();
                data[i][1] = bookMetadata.getSeller() != null ? bookMetadata.getSeller().getName() : "На складе";
            } else if (clazz.equals(SellerMetadata.class)) {
                SellerMetadata sellerMetadata = (SellerMetadata) booksMetadata.get(i);
                data[i][0] = sellerMetadata.getCountOfBook();
                data[i][1] = sellerMetadata.getBook().getName();
            }

        }
        setDataVector(data, columnNames);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
}
